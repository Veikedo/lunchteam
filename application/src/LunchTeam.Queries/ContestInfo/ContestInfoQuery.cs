﻿using MediatR;

namespace LunchTeam.Queries.ContestInfo
{
    public class ContestInfoQuery : IRequest<ContestInfoQueryResult>
    {
    }
}