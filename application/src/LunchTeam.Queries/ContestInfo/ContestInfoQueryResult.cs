﻿using System;
using System.Collections.Generic;

namespace LunchTeam.Queries.ContestInfo
{
    public class ContestInfoQueryResult
    {
        public DateTime NextDinnerDate { get; set; }
        public int MembersNumber { get; set; }
        public List<ContestMember> LastContestWinners { get; set; }
        public DateTime? LastContestDinnerDate { get; set; }
    }
}