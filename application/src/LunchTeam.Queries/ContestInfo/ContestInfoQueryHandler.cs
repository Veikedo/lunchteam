﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Dal;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Queries.ContestInfo
{
    public class ContestInfoQueryHandler : IAsyncRequestHandler<ContestInfoQuery, ContestInfoQueryResult>
    {
        private readonly LunchTeamDb _db;

        public ContestInfoQueryHandler(LunchTeamDb db)
        {
            _db = db;
        }

        public async Task<ContestInfoQueryResult> Handle(ContestInfoQuery message)
        {
            var currentContest = await _db.Contests
                .GetCurrentContestAsync(c => new {c.Id, c.DinnerDate})
                .ConfigureAwait(false);

            if (currentContest == null)
            {
                return null;
            }

            int membersNumber = await _db.ContestSagas
                .CountAsync(cs => cs.ContestId == currentContest.Id)
                .ConfigureAwait(false);

            var lastCompletedContestInfo = await GetLastCompletedContestInfoAsync().ConfigureAwait(false);

            return new ContestInfoQueryResult {
                NextDinnerDate = currentContest.DinnerDate,
                MembersNumber = membersNumber,
                LastContestWinners = lastCompletedContestInfo.Winners,
                LastContestDinnerDate = lastCompletedContestInfo.DinnerDate
            };
        }

        private async Task<LastCompletedContestInfo> GetLastCompletedContestInfoAsync()
        {
            var lastCompletedContest = await _db.Contests
                .GetLastCompletedContest(c => new {c.Id, c.DinnerDate})
                .ConfigureAwait(false);

            if (lastCompletedContest == null)
            {
                return new LastCompletedContestInfo();
            }

            var winnersQuery = from saga in _db.ContestSagas
                               join user in _db.Users on saga.UserLogin equals user.Login
                               where saga.ContestId == lastCompletedContest.Id && saga.IsWinner
                               select new ContestMember {
                                   FirstName = user.FirstName,
                                   LastName = user.LastName,
                                   JobPosition = user.JobPosition,
                                   Login = user.Login,
                                   HasAuto = saga.IsDriver
                               };

            var winners = await winnersQuery.ToListAsync().ConfigureAwait(false);

            return new LastCompletedContestInfo {
                DinnerDate = lastCompletedContest.DinnerDate,
                Winners = winners
            };
        }

        private class LastCompletedContestInfo
        {
            public List<ContestMember> Winners { get; set; }
            public DateTime? DinnerDate { get; set; }
        }
    }
}