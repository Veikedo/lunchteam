namespace LunchTeam.Queries.ContestInfo
{
    public class ContestMember
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobPosition { get; set; }
        public bool HasAuto { get; set; }
    }
}