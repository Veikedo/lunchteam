using System;
using System.Collections.Generic;
using LunchTeam.Domain;

namespace LunchTeam.Queries.UserQuestions
{
    public class UserQuestionsQueryResult
    {
        public List<Question> Questions { get; set; }

        public class Question
        {
            public Guid Id { get; set; }
            public Guid? CorrectVariantId { get; set; }
            public Guid? AnsweredVariantId { get; set; }
            public List<Variant> Variants { get; set; }
        }

        public class Variant
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobPosition { get; set; }
        }
    }
}