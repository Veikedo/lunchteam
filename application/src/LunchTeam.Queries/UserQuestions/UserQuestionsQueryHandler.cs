﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using LunchTeam.Core;
using LunchTeam.Dal;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Queries.UserQuestions
{
    public class UserQuestionsQueryHandler : IAsyncRequestHandler<UserQuestionsQuery, UserQuestionsQueryResult>
    {
        private readonly LunchTeamDb _db;
        private readonly IIdentityProvider _identityProvider;

        public UserQuestionsQueryHandler(LunchTeamDb db, IIdentityProvider identityProvider)
        {
            _db = db;
            _identityProvider = identityProvider;
        }

        public async Task<UserQuestionsQueryResult> Handle(UserQuestionsQuery message)
        {
            var contestId = await _db.Contests.GetCurrentContestIdAsync().ConfigureAwait(false);
            string userLogin = _identityProvider.GetCurrentUserLogin();

            return await _db.ContestSagas
                .Where(cs => cs.ContestId == contestId && cs.UserLogin == userLogin)
                .ProjectTo<UserQuestionsQueryResult>()
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);
        }
    }
}