﻿using System.Threading.Tasks;
using MediatR;

namespace LunchTeam.Queries.UserQuestions
{
    public class UserQuestionsQuery : IRequest<UserQuestionsQueryResult>
    {
    }

    public class QueryDb : IRequest<object>
    {
    }

    public class QueryDbHandler : IRequestHandler<QueryDb, object>
    {
        public object Handle(QueryDb message)
        {
            var o = new object();
            return o;
        }
    }

    public class TestQueryDb : QueryDb
    {
    }
}