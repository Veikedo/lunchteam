﻿using System;
using AutoMapper;
using LunchTeam.Domain;

namespace LunchTeam.Queries.UserQuestions
{
    public class UserQuestionMapperProfile : Profile
    {
        public UserQuestionMapperProfile()
        {
            CreateMap<ContestSaga, UserQuestionsQueryResult>();

            CreateMap<Question, UserQuestionsQueryResult.Question>()
                .ForMember(dest => dest.CorrectVariantId,
                    opt => opt.MapFrom(src => src.IsAnswered ? (Guid?) src.CorrectVariantId : null))
                ;

            CreateMap<Variant, UserQuestionsQueryResult.Variant>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.HiddenUser.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.HiddenUser.LastName))
                .ForMember(dest => dest.JobPosition, opt => opt.MapFrom(src => src.HiddenUser.JobPosition))
                ;
        }
    }
}