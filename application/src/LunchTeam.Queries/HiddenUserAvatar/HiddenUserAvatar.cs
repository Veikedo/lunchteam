﻿namespace LunchTeam.Queries.HiddenUserAvatar
{
    public class HiddenUserAvatar
    {
        public bool IsCropped { get; set; }
        public byte[] Image { get; set; }
    }
}