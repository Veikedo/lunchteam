﻿using System;
using LunchTeam.Domain;
using MediatR;

namespace LunchTeam.Queries.HiddenUserAvatar
{
    public class HiddenUserAvatarQuery : IRequest<HiddenUserAvatar>
    {
        public HiddenUserAvatarQuery(Guid questionId)
        {
            QuestionId = questionId;
        }

        public Guid QuestionId { get; }
    }
}