﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LunchTeam.Dal;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Queries.HiddenUserAvatar
{
    public class HiddenUserAvatarQueryHandler : IAsyncRequestHandler<HiddenUserAvatarQuery, HiddenUserAvatar>
    {
        private readonly IAvatarService _avatarService;
        private readonly LunchTeamDb _db;
        private readonly IMapper _mapper;

        public HiddenUserAvatarQueryHandler(IAvatarService avatarService, LunchTeamDb db, IMapper mapper)
        {
            _avatarService = avatarService;
            _db = db;
            _mapper = mapper;
        }

        public async Task<HiddenUserAvatar> Handle(HiddenUserAvatarQuery message)
        {
            var query = from question in _db.Questions
                        join variant in _db.Variants on question.CorrectVariantId equals variant.Id
                        where question.Id == message.QuestionId
                        select variant.HiddenUserLogin;

            string userLogin = await query.FirstAsync().ConfigureAwait(false);
            var avatar = await _avatarService.GetUserAvatarAsync(userLogin).ConfigureAwait(false);

            return _mapper.Map<HiddenUserAvatar>(avatar);
        }
    }
}