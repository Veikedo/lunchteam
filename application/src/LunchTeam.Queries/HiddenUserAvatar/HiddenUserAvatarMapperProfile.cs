﻿using AutoMapper;
using LunchTeam.Domain;

namespace LunchTeam.Queries.HiddenUserAvatar
{
    public class HiddenUserAvatarMapperProfile : Profile
    {
        public HiddenUserAvatarMapperProfile()
        {
            CreateMap<Avatar, HiddenUserAvatar>();
        }
    }
}