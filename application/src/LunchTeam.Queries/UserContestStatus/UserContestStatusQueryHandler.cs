﻿using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Core;
using LunchTeam.Dal;
using LunchTeam.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Queries.UserContestStatus
{
    public class UserContestStatusQueryHandler : IAsyncRequestHandler<UserContestStatusQuery, UserContestStatusResult>
    {
        private readonly LunchTeamDb _db;
        private readonly IIdentityProvider _identityProvider;

        public UserContestStatusQueryHandler(LunchTeamDb db, IIdentityProvider identityProvider)
        {
            _db = db;
            _identityProvider = identityProvider;
        }

        public async Task<UserContestStatusResult> Handle(UserContestStatusQuery message)
        {
            string currentUserName = _identityProvider.GetCurrentUserLogin();
            var currentContestId = await _db.Contests.GetCurrentContestIdAsync().ConfigureAwait(false);

            var state = await _db.ContestSagas
                .Where(cs => cs.ContestId == currentContestId && cs.UserLogin == currentUserName)
                .Select(cs => cs.State)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);

            return new UserContestStatusResult(state);
        }
    }
}