﻿using MediatR;

namespace LunchTeam.Queries.UserContestStatus
{
    public class UserContestStatusQuery : IRequest<UserContestStatusResult>
    {
    }
}