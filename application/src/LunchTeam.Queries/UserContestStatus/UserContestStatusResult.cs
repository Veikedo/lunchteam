﻿using LunchTeam.Domain;

namespace LunchTeam.Queries.UserContestStatus
{
    public class UserContestStatusResult
    {
        public UserContestStatusResult(ContestSaga.States state)
        {
            State = state;
        }

        public ContestSaga.States State { get; }
    }
}