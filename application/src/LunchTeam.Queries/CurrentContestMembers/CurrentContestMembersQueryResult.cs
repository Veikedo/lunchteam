﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Queries.ContestInfo;
using MediatR;

namespace LunchTeam.Queries.CurrentContestMembers
{
    public class CurrentContestMembersQueryResult
    {
        public List<ContestMember> Members { get; set; }
    }

    public class CurrentContestMembersQueryHandler : IAsyncRequestHandler<CurrentContestMembersQuery, CurrentContestMembersQueryResult>
    {
        private readonly LunchTeamDb _db;

        public CurrentContestMembersQueryHandler(LunchTeamDb db)
        {
            _db = db;
        }

        public Task<CurrentContestMembersQueryResult> Handle(CurrentContestMembersQuery message)
        {
            throw new NotImplementedException();
        }
    }
}