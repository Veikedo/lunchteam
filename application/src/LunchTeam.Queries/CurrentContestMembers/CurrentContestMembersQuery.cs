﻿using MediatR;

namespace LunchTeam.Queries.CurrentContestMembers
{
    public class CurrentContestMembersQuery : IRequest<CurrentContestMembersQueryResult>
    {
    }
}