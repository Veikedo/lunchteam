﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Core;
using LunchTeam.Dal.Extensions;
using LunchTeam.Domain;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal
{
    public static class UserRepository
    {
        public static async Task SynchronizeAsync(this DbSet<User> set, IReadOnlyList<User> changedUsers)
        {
            var databaseUsers = await set
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            var lookup = changedUsers
                .ToLookup(changed => databaseUsers.Any(db => string.Equals(db.Login, changed.Login, StringComparison.OrdinalIgnoreCase)));

            var updatedUsers = lookup[true];
            var newUsers = lookup[false];
            var removedUsers = databaseUsers
                .Where(db => changedUsers.All(changed => changed.Login != db.Login))
                .Do(db => db.Deactivate());

            set.UpdateRange(updatedUsers.Concat(removedUsers));
            set.AddRange(newUsers);
        }
    }
}