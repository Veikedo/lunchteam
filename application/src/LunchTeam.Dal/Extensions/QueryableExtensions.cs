﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Core;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal.Extensions
{
    public static class QueryableExtensions
    {
        public static Task<HashSet<T>> ToHashSetAsync<T>(this IQueryable<T> source)
        {
            return source.ToListAsync().ContinueWith(t => t.Result.ToHashSet());
        }

        public static HashSet<T> ToHashSet<T>(this IQueryable<T> source)
        {
            return new HashSet<T>(source);
        }
    }
}