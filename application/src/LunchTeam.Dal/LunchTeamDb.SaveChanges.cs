﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LunchTeam.Core;
using LunchTeam.Domain.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal
{
    public partial class LunchTeamDb
    {
        public override int SaveChanges()
        {
            int result = base.SaveChanges();
            // i can do it because i use .ConfigureAwait(false) in the method
            PublishEventsAsync().Wait();

            return result;
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            int result = base.SaveChanges(acceptAllChangesOnSuccess);
            PublishEventsAsync().Wait();
            return result;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            await PublishEventsAsync().ConfigureAwait(false);

            return result;
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new CancellationToken())
        {
            int result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken).ConfigureAwait(false);
            await PublishEventsAsync().ConfigureAwait(false);

            return result;
        }

        private async Task PublishEventsAsync()
        {
            var entries = ChangeTracker
                .Entries<IHasEvents>()
                .Select(e => e.Entity)
                .ToList();
            
            foreach (var entry in entries)
            {
                Entry(entry).State = EntityState.Detached;
                // use dynamic because we need an underlying type of interface
                // mediatr issue https://github.com/jbogard/MediatR/issues/133
                foreach (dynamic message in entry.Events)
                {
                    await _mediator.Publish(message).ConfigureAwait(false);
                }
            }
        }
    }
}