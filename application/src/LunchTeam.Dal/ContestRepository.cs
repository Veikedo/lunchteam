﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LunchTeam.Domain;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal
{
    public static class ContestRepository
    {
        public static Task<TResult> GetLastCompletedContest<TResult>(this IQueryable<Contest> q,
            Expression<Func<Contest, TResult>> selector)
        {
            return q
                .Where(c => c.IsCompleted)
                .OrderByDescending(c => c.DinnerDate)
                .Select(selector)
                .FirstOrDefaultAsync();
        }

        public static Task<TResult> GetCurrentContestAsync<TResult>(this IQueryable<Contest> q, Expression<Func<Contest, TResult>> selector)
        {
            return q
                .Where(c => !c.IsCompleted)
                .OrderBy(c => c.DinnerDate)
                .Select(selector)
                .FirstOrDefaultAsync();
        }

        public static Task<Contest> GetCurrentContestAsync(this IQueryable<Contest> q)
        {
            return q
                .Where(c => !c.IsCompleted)
                .OrderBy(c => c.DinnerDate)
                .Select(c => c)
                .FirstOrDefaultAsync();
        }

        public static Task<Guid> GetCurrentContestIdAsync(this IQueryable<Contest> q)
        {
            return q.GetCurrentContestAsync(c => c.Id);
        }

        public static Task<DateTime> GetCurrentContestDinnerDateAsync(this IQueryable<Contest> q)
        {
            return q.GetCurrentContestAsync(c => c.DinnerDate);
        }
    }
}