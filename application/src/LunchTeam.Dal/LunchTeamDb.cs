﻿using LunchTeam.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal
{
    public partial class LunchTeamDb : DbContext
    {
        private readonly IMediator _mediator;

        public LunchTeamDb(DbContextOptions<LunchTeamDb> options, IMediator mediator) : base(options)
        {
            _mediator = mediator;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Contest> Contests { get; set; }
        public DbSet<ContestSaga> ContestSagas { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Variant> Variants { get; set; }
    }
}