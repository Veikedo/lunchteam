﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchTeam.Dal.Migrations
{
    public partial class v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContestSagas_Users_UserLogin2",
                table: "ContestSagas");

            migrationBuilder.DropIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants");

            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_ContestSagas_UserLogin2",
                table: "ContestSagas");

            migrationBuilder.DropColumn(
                name: "UserLogin2",
                table: "ContestSagas");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants",
                column: "HiddenUserLogin");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants");

            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.AddColumn<string>(
                name: "UserLogin2",
                table: "ContestSagas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants",
                column: "HiddenUserLogin",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_UserLogin2",
                table: "ContestSagas",
                column: "UserLogin2");

            migrationBuilder.AddForeignKey(
                name: "FK_ContestSagas_Users_UserLogin2",
                table: "ContestSagas",
                column: "UserLogin2",
                principalTable: "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
