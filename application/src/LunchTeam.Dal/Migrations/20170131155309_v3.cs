﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchTeam.Dal.Migrations
{
    public partial class v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants");

            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_ContestSagas_UserLogin",
                table: "ContestSagas");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants",
                column: "HiddenUserLogin");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_UserLogin",
                table: "ContestSagas",
                column: "UserLogin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants");

            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_ContestSagas_UserLogin",
                table: "ContestSagas");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants",
                column: "HiddenUserLogin",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_UserLogin",
                table: "ContestSagas",
                column: "UserLogin",
                unique: true);
        }
    }
}
