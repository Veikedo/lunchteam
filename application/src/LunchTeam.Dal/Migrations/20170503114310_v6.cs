﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchTeam.Dal.Migrations
{
    public partial class v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Avatar");

            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.CreateTable(
                name: "Avatar",
                columns: table => new
                {
                    UserLogin = table.Column<string>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    IsCropped = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avatar", x => x.UserLogin);
                    table.ForeignKey(
                        name: "FK_Avatar_Users_UserLogin",
                        column: x => x.UserLogin,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId",
                unique: true);
        }
    }
}
