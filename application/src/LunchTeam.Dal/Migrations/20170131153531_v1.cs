﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchTeam.Dal.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompletionDate = table.Column<DateTime>(nullable: false),
                    DinnerDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Login = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    JobPosition = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    RegistrationDate = table.Column<DateTime>(nullable: false),
                    Sex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Login);
                });

            migrationBuilder.CreateTable(
                name: "Avatar",
                columns: table => new
                {
                    UserLogin = table.Column<string>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    IsCropped = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avatar", x => x.UserLogin);
                    table.ForeignKey(
                        name: "FK_Avatar_Users_UserLogin",
                        column: x => x.UserLogin,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContestSagas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ContestId = table.Column<Guid>(nullable: false),
                    HasAuto = table.Column<bool>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    UserLogin = table.Column<string>(nullable: false),
                    UserLogin2 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestSagas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContestSagas_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContestSagas_Users_UserLogin",
                        column: x => x.UserLogin,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContestSagas_Users_UserLogin2",
                        column: x => x.UserLogin2,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnsweredVariantId = table.Column<Guid>(nullable: true),
                    ContestSagaId = table.Column<Guid>(nullable: false),
                    CorrectVariantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_ContestSagas_ContestSagaId",
                        column: x => x.ContestSagaId,
                        principalTable: "ContestSagas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    HiddenUserLogin = table.Column<string>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Variants_Users_HiddenUserLogin",
                        column: x => x.HiddenUserLogin,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Variants_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_ContestId",
                table: "ContestSagas",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_UserLogin",
                table: "ContestSagas",
                column: "UserLogin",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContestSagas_UserLogin2",
                table: "ContestSagas",
                column: "UserLogin2");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_ContestSagaId",
                table: "Questions",
                column: "ContestSagaId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_CorrectVariantId",
                table: "Questions",
                column: "CorrectVariantId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Variants_HiddenUserLogin",
                table: "Variants",
                column: "HiddenUserLogin");

            migrationBuilder.CreateIndex(
                name: "IX_Variants_QuestionId",
                table: "Variants",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Questions_Variants_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId",
                principalTable: "Variants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContestSagas_Users_UserLogin",
                table: "ContestSagas");

            migrationBuilder.DropForeignKey(
                name: "FK_ContestSagas_Users_UserLogin2",
                table: "ContestSagas");

            migrationBuilder.DropForeignKey(
                name: "FK_Variants_Users_HiddenUserLogin",
                table: "Variants");

            migrationBuilder.DropForeignKey(
                name: "FK_ContestSagas_Contests_ContestId",
                table: "ContestSagas");

            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Variants_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.DropTable(
                name: "Avatar");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Contests");

            migrationBuilder.DropTable(
                name: "Variants");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "ContestSagas");
        }
    }
}
