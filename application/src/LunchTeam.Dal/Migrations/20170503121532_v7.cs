﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LunchTeam.Dal.Migrations
{
    public partial class v7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "IsSynchronizedWithRedmine",
                table: "Users");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions");

            migrationBuilder.AddColumn<bool>(
                name: "IsSynchronizedWithRedmine",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_AnsweredVariantId",
                table: "Questions",
                column: "AnsweredVariantId",
                unique: true);
        }
    }
}
