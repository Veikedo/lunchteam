﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using LunchTeam.Dal;
using LunchTeam.Domain;

namespace LunchTeam.Dal.Migrations
{
    [DbContext(typeof(LunchTeamDb))]
    [Migration("20170504075028_added IsDriver to ContestSaga")]
    partial class addedIsDrivertoContestSaga
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("LunchTeam.Domain.Contest", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DinnerDate");

                    b.Property<bool>("IsCompleted");

                    b.HasKey("Id");

                    b.ToTable("Contests");
                });

            modelBuilder.Entity("LunchTeam.Domain.ContestSaga", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ContestId");

                    b.Property<bool>("HasAuto");

                    b.Property<bool>("IsDriver");

                    b.Property<int>("State");

                    b.Property<string>("UserLogin")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("ContestId");

                    b.HasIndex("UserLogin");

                    b.ToTable("ContestSagas");
                });

            modelBuilder.Entity("LunchTeam.Domain.Question", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AnsweredVariantId");

                    b.Property<Guid>("ContestSagaId");

                    b.Property<Guid>("CorrectVariantId");

                    b.HasKey("Id");

                    b.HasIndex("AnsweredVariantId");

                    b.HasIndex("ContestSagaId");

                    b.HasIndex("CorrectVariantId")
                        .IsUnique();

                    b.ToTable("Questions");
                });

            modelBuilder.Entity("LunchTeam.Domain.User", b =>
                {
                    b.Property<string>("Login")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<bool>("IsActive");

                    b.Property<string>("JobPosition");

                    b.Property<string>("LastName");

                    b.Property<DateTime>("RegistrationDate");

                    b.Property<int>("Sex");

                    b.HasKey("Login");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("LunchTeam.Domain.Variant", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("HiddenUserLogin")
                        .IsRequired();

                    b.Property<Guid?>("QuestionId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("HiddenUserLogin");

                    b.HasIndex("QuestionId");

                    b.ToTable("Variants");
                });

            modelBuilder.Entity("LunchTeam.Domain.ContestSaga", b =>
                {
                    b.HasOne("LunchTeam.Domain.Contest", "Contest")
                        .WithMany()
                        .HasForeignKey("ContestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("LunchTeam.Domain.User")
                        .WithMany()
                        .HasForeignKey("UserLogin")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LunchTeam.Domain.Question", b =>
                {
                    b.HasOne("LunchTeam.Domain.Variant")
                        .WithOne()
                        .HasForeignKey("LunchTeam.Domain.Question", "AnsweredVariantId");

                    b.HasOne("LunchTeam.Domain.ContestSaga")
                        .WithMany("Questions")
                        .HasForeignKey("ContestSagaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("LunchTeam.Domain.Variant", b =>
                {
                    b.HasOne("LunchTeam.Domain.User", "HiddenUser")
                        .WithMany()
                        .HasForeignKey("HiddenUserLogin");

                    b.HasOne("LunchTeam.Domain.Question")
                        .WithMany("Variants")
                        .HasForeignKey("QuestionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
