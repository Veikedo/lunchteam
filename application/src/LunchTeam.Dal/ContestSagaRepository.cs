using System;
using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Domain;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Dal
{
    public static class ContestSagaRepository
    {
        public static Task<bool> IsUserJoinedContest(this IQueryable<ContestSaga> q, string userLogin, Guid contestId)
        {
            return q.AnyAsync(cs => cs.ContestId == contestId && cs.UserLogin == userLogin);
        }

        public static Task<ContestSaga> GetAsync(this DbSet<ContestSaga> db, Guid id)
        {
            return db
                .Where(cs => cs.Id == id)
                .Include(cs => cs.Questions).ThenInclude(q => q.Variants)
                .Include(cs => cs.Contest)
                .FirstOrDefaultAsync();
        }
    }
}