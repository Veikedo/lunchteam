﻿using LunchTeam.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LunchTeam.Dal
{
    public partial class LunchTeamDb
    {
        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<User>()
                .HasKey(u => u.Login);

            mb.Entity<ContestSaga>()
                .HasOne(cs => cs.Contest)
                .WithMany()
                .HasForeignKey(cs => cs.ContestId)
                .IsRequired();

            mb.Entity<ContestSaga>()
                .HasOne(typeof(User))
                .WithMany()
                .HasForeignKey("UserLogin")
                .IsRequired();

            mb.Entity<ContestSaga>()
                .HasMany(cs => cs.Questions)
                .WithOne()
                .HasForeignKey(q => q.ContestSagaId)
                .IsRequired();

            mb.Entity<Variant>()
                .ToTable("Variants");

            mb.Entity<Question>()
                .HasMany(q => q.Variants)
                .WithOne()
                .HasForeignKey("QuestionId")
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            mb.Entity<Question>()
                .HasOne(typeof(Variant))
                .WithOne()
                .HasForeignKey<Question>("AnsweredVariantId")
                .IsRequired(false) 
                .OnDelete(DeleteBehavior.Restrict);

            // EF creates Unique Index for nullable fields
            mb.Entity<Question>()
                .HasIndex(q => q.AnsweredVariantId)
                .IsUnique(false);

            // create index instead of FK hence the cyclic dependency between Question and Variant
            mb.Entity<Question>()
                .HasIndex(q => q.CorrectVariantId)
                .IsUnique();

            mb.Entity<Variant>()
                .HasOne(v => v.HiddenUser)
                .WithMany()
                .HasForeignKey(v => v.HiddenUserLogin)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}