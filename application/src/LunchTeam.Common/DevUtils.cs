﻿using System;

namespace LunchTeam.Common
{
    public static class DevUtils
    {
        private const string Development = "Development";
        public static bool IsDevelopment() => Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Development;
    }
}