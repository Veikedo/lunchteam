﻿using System.IO;
using System.Threading.Tasks;

namespace LunchTeam.Common
{
    public class FileUtils
    {
        public static async Task<byte[]> ReadAllFileAsync(string filename)
        {
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true))
            {
                var buff = new byte[file.Length];
                await file.ReadAsync(buff, 0, (int) file.Length).ConfigureAwait(false);

                return buff;
            }
        }

        public static byte[] ReadAllFile(string filename)
        {
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true))
            {
                var buff = new byte[file.Length];
                file.Read(buff, 0, (int) file.Length);

                return buff;
            }
        }
    }
}