﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace LunchTeam.Common
{
    public static class RandomUtils
    {
        private static readonly Random Random = new Random();

        public static T GetRandomItem<T>(this IEnumerable<T> source)
        {
            var list = source as IList<T> ?? source.ToList();
            if (list.Count == 0)
            {
                return default(T);
            }

            int idx = Random.Next(0, list.Count);
            return list[idx];
        }

        public static IReadOnlyList<T> Shuffle<T>(this IEnumerable<T> source)
        {
            var list = source.ToList();

            var provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                var box = new byte[1];
                do
                {
                    provider.GetBytes(box);
                } while (!(box[0] < n * (byte.MaxValue / n)));
                int k = box[0] % n;
                n--;
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        public static IReadOnlyList<T> GetRandomItems<T>(this IEnumerable<T> source, int count)
        {
            // todo optimize it
            return source.Shuffle().Take(count).ToList();
        }
    }
}