﻿using System.Threading.Tasks;
using LunchTeam.Services.Contracts;
using MediatR;

namespace LunchTeam.Commands
{
    public class StartSyncWithRedmineHandler : IAsyncRequestHandler<StartSyncWithRedmine>
    {
        private readonly IUserSynchronizer _userSynchronizer;

        public StartSyncWithRedmineHandler(IUserSynchronizer userSynchronizer)
        {
            _userSynchronizer = userSynchronizer;
        }

        public Task Handle(StartSyncWithRedmine message)
        {
            return _userSynchronizer.SynchronizeAsync();
        }
    }
}