﻿using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Services.Contracts;
using MediatR;

namespace LunchTeam.Commands
{
    public class NotifyNewContestStartedHandler : IAsyncRequestHandler<NotifyNewContestStarted>
    {
        private readonly LunchTeamDb _db;
        private readonly INotificationService _notificationService;

        public NotifyNewContestStartedHandler(LunchTeamDb db, INotificationService notificationService)
        {
            _db = db;
            _notificationService = notificationService;
        }

        public async Task Handle(NotifyNewContestStarted message)
        {
            var dinnerDate = await _db.Contests
                .GetCurrentContestDinnerDateAsync()
                .ConfigureAwait(false);

            await _notificationService.NotifyNewContestStartedAsync(dinnerDate).ConfigureAwait(false);
        }
    }
}