﻿using System.Threading.Tasks;
using LunchTeam.Services.Contracts;
using MediatR;

namespace LunchTeam.Commands
{
    public class AddUserToCurrentContestHandler : IAsyncRequestHandler<AddUserToCurrentContest>
    {
        private readonly IContestService _contestService;

        public AddUserToCurrentContestHandler(IContestService contestService)
        {
            _contestService = contestService;
        }

        public Task Handle(AddUserToCurrentContest message)
        {
            return _contestService.AddUserToCurrentContestAsync();
        }
    }
}