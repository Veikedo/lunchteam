﻿using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Commands
{
    public class AnswerQuestionHandler : IAsyncRequestHandler<AnswerQuestion, AnswerResult>
    {
        private readonly LunchTeamDb _db;

        public AnswerQuestionHandler(LunchTeamDb db)
        {
            _db = db;
        }

        public async Task<AnswerResult> Handle(AnswerQuestion message)
        {
            var question = await _db.Questions
                .FirstOrDefaultAsync(q => q.Id == message.QuestionId)
                .ConfigureAwait(false);

            var result = question?.Answer(message.VariantId) ?? AnswerResult.Failed();

            await _db.SaveChangesAsync().ConfigureAwait(false);
            return result;
        }
    }
}