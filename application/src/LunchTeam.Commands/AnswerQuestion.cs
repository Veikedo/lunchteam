﻿using System;
using LunchTeam.Domain;
using MediatR;

namespace LunchTeam.Commands
{
    public class AnswerQuestion : IRequest<AnswerResult>
    {
        public Guid QuestionId { get; set; }
        public Guid VariantId { get; set; }
    }
} 