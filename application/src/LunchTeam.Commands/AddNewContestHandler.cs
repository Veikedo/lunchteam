﻿using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Domain;
using LunchTeam.Domain.Events;
using MediatR;

namespace LunchTeam.Commands
{
    public class AddNewContestHandler : IAsyncRequestHandler<AddNewContest, Contest>
    {
        private readonly LunchTeamDb _db;
        private readonly IMediator _mediator;

        public AddNewContestHandler(LunchTeamDb db, IMediator mediator)
        {
            _db = db;
            _mediator = mediator;
        }

        public async Task<Contest> Handle(AddNewContest message)
        {
            var contest = new Contest(message.DinnerDate);

            _db.Add(contest);
            await _db.SaveChangesAsync().ConfigureAwait(false);

            await _mediator.Publish(new NewContestAdded(contest)).ConfigureAwait(false);

            return contest;
        }
    }
}