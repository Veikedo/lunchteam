﻿using System.Threading.Tasks;
using LunchTeam.Services.Contracts;
using MediatR;

namespace LunchTeam.Commands
{
    public class CompleteCurrentContestHandler : IAsyncRequestHandler<CompleteCurrentContest>
    {
        private readonly IContestService _contestService;

        public CompleteCurrentContestHandler(IContestService contestService)
        {
            _contestService = contestService;
        }

        public Task Handle(CompleteCurrentContest message)
        {
            return _contestService.CompleteCurrentContestAsync();
        }
    }
}