﻿using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Domain.Events;
using MediatR;

namespace LunchTeam.Commands.EventHandlers
{
    public class QuestionAnsweredRouter : IAsyncNotificationHandler<QuestionAnswered>
    {
        private readonly LunchTeamDb _db;

        public QuestionAnsweredRouter(LunchTeamDb db)
        {
            _db = db;
        }

        public async Task Handle(QuestionAnswered message)
        {
            var contestSaga = await _db.ContestSagas
                .GetAsync(message.ContestSagaId)
                .ConfigureAwait(false);

            contestSaga.Handle(message);

            await _db.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}