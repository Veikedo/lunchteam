﻿using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Dal;
using LunchTeam.Domain.Events;
using LunchTeam.Services.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Commands.EventHandlers
{
    public class ContestCompletedRouter : IAsyncNotificationHandler<ContestCompleted>
    {
        private readonly LunchTeamDb _db;
        private readonly INotificationService _notificationService;

        public ContestCompletedRouter(LunchTeamDb db, INotificationService notificationService)
        {
            _db = db;
            _notificationService = notificationService;
        }

        public async Task Handle(ContestCompleted message)
        {
            var contest = await _db.Contests
                .GetCurrentContestAsync()
                .ConfigureAwait(false);

            contest.Complete();

            var contestSagas = await _db.ContestSagas
                .Where(cs => cs.ContestId == message.ContestId)
                .ToListAsync()
                .ConfigureAwait(false);

            foreach (var contestSaga in contestSagas)
            {
                contestSaga.Complete(message.Winners);
            }

            await _notificationService
                .NotifyContestCompletedAsync(message.Winners)
                .ConfigureAwait(false);

            await _db.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}