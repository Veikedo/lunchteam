﻿using System;
using System.Threading.Tasks;
using Hangfire;
using LunchTeam.Domain.Events;
using MediatR;

namespace LunchTeam.Commands.EventHandlers
{
    public class NewContestAddedHandler : IAsyncNotificationHandler<NewContestAdded>
    {
        private readonly IBackgroundJobClient _jobClient;
        private readonly IMediator _mediator;

        public NewContestAddedHandler(IMediator mediator, IBackgroundJobClient jobClient)
        {
            _mediator = mediator;
            _jobClient = jobClient;
        }

        public Task Handle(NewContestAdded notification)
        {
            var date = notification.Contest.DinnerDate - TimeSpan.FromDays(2);
            var invitationDateTime = new DateTime(date.Year, date.Month, date.Day, 10, 0, 0);

            date = notification.Contest.DinnerDate - TimeSpan.FromDays(1);
            var resultDateTime = new DateTime(date.Year, date.Month, date.Day, 12, 0, 0);

            _jobClient.Schedule<NewContestAddedHandler>(h => h.NotifyNewContestStartedAsync(), invitationDateTime);
            _jobClient.Schedule<NewContestAddedHandler>(h => h.CompleteCurrentContestAsync(), resultDateTime);

            return Task.CompletedTask;
        }

        public Task CompleteCurrentContestAsync() => _mediator.Send(new CompleteCurrentContest());

        public Task NotifyNewContestStartedAsync() => _mediator.Send(new NotifyNewContestStarted());
    }
}