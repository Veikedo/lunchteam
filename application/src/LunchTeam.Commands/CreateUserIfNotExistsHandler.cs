﻿using System.Threading.Tasks;
using LunchTeam.Core;
using LunchTeam.Dal;
using LunchTeam.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Commands
{
    public class CreateUserIfNotExistsHandler : IAsyncRequestHandler<CreateUserIfNotExists>
    {
        private readonly LunchTeamDb _db;
        private readonly IIdentityProvider _identityProvider;

        public CreateUserIfNotExistsHandler(LunchTeamDb db, IIdentityProvider identityProvider)
        {
            _db = db;
            _identityProvider = identityProvider;
        }

        public async Task Handle(CreateUserIfNotExists message)
        {
            string userLogin = _identityProvider.GetCurrentUserLogin();
            bool notExists = await _db.Users.AllAsync(x => x.Login != userLogin).ConfigureAwait(false);

            if (notExists)
            {
                _db.Users.Add(new User(userLogin));
                await _db.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}