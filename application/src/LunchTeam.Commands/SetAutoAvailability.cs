﻿using System;
using System.Linq;
using MediatR;

namespace LunchTeam.Commands
{
    public class SetAutoAvailability : IRequest
    {
        public bool HasAuto { get; set; }
    }
}