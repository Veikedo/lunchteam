﻿using System;
using LunchTeam.Domain;
using MediatR;

namespace LunchTeam.Commands
{
    public class AddNewContest : IRequest<Contest>
    {
        public DateTime DinnerDate { get;  set; }
    }
}