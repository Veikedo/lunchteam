﻿using System.Threading.Tasks;
using LunchTeam.Core;
using LunchTeam.Dal;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Commands
{
    public class SetAutoAvailabilityHandler : IAsyncRequestHandler<SetAutoAvailability>
    {
        private readonly LunchTeamDb _db;
        private readonly IIdentityProvider _identityProvider;

        public SetAutoAvailabilityHandler(LunchTeamDb db, IIdentityProvider identityProvider)
        {
            _db = db;
            _identityProvider = identityProvider;
        }

        public async Task Handle(SetAutoAvailability message)
        {
            string userLogin = _identityProvider.GetCurrentUserLogin();
            var contestId = await _db.Contests.GetCurrentContestIdAsync().ConfigureAwait(false);
            var saga = await _db.ContestSagas
                .FirstOrDefaultAsync(cs => cs.UserLogin == userLogin && cs.ContestId == contestId)
                .ConfigureAwait(false);

            saga.HasAuto = message.HasAuto;
            await _db.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}