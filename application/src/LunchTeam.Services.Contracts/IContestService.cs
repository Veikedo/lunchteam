﻿using System.Threading.Tasks;

namespace LunchTeam.Services.Contracts
{
    public interface IContestService
    {
        Task CompleteCurrentContestAsync();
        Task AddUserToCurrentContestAsync();
    }
}