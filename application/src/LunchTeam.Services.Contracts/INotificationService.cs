﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LunchTeam.Domain;

namespace LunchTeam.Services.Contracts
{
    public interface INotificationService
    {
        Task NotifyContestCompletedAsync(IReadOnlyList<UserContestInfo> winners);
        Task NotifyNewContestStartedAsync(DateTime dinnerDate);
    }
}