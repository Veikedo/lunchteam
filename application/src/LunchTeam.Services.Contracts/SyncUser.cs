﻿using System;
using LunchTeam.Domain;

namespace LunchTeam.Services.Contracts
{
    public class SyncUser
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobPosition { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Email { get; set; }
        public Sex? Sex { get; set; }
        public byte[] Avatar { get; set; }

        /// <summary>
        ///     this is to prevent Build Bots, Team City, etc
        /// </summary>
        public bool IsHuman => Email != null && JobPosition != null && FirstName != null && LastName != null;
    }
}