﻿using System.Threading.Tasks;
using LunchTeam.Domain;

namespace LunchTeam.Services.Contracts
{
    public interface IAvatarService
    {
        Task<Avatar> GetUserAvatarAsync(string userLogin);
        void SaveUserAvatar(Avatar avatar);
        bool DoesUserHaveAvatar(string userLogin);
    }
}