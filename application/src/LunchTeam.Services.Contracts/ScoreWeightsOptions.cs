﻿namespace LunchTeam.Services.Contracts
{
    public class ScoreWeightsOptions
    {
        public double Victory { get; set; }
        public double Registration { get; set; }
        public double Answers { get; set; }
    }
}