using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchTeam.Services.Contracts
{
    public interface ILDAPService
    {
        Task<List<SyncUser>> GetAllUsersAsync();
    }
}