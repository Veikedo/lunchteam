﻿namespace LunchTeam.Services.Contracts
{
    public class RedmineOptions
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string ApiKey { get; set; }
        public string PlayersGroupId { get; set; }
        public string GirlsGroupId { get; set; }
    }
}