﻿namespace LunchTeam.Services.Contracts
{
    public class SmtpOptions
    {
        public string Host { get; set; }
        public string Login { get; set; }
        public string From { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string DevAddress { get; set; }
        public string AllAddress { get; set; }
    }
}