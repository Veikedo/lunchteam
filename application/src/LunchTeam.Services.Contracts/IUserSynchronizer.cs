﻿using System.Threading.Tasks;

namespace LunchTeam.Services.Contracts
{
    public interface IUserSynchronizer
    {
        Task SynchronizeAsync();
    }
}