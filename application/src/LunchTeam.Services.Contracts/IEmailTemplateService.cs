﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LunchTeam.Domain;

namespace LunchTeam.Services.Contracts
{
    public interface IEmailTemplateService
    {
        Task<string> NewContestTemplateAsync(DateTime dinnerDate);
        Task<string> ContestCompletedTemplateAsync(IReadOnlyList<UserContestInfo> winners);
        Task<string> ForContestWinnersTemplateAsync(IReadOnlyList<UserContestInfo> winners);
    }
}