﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LunchTeam.Services.Contracts
{
    public interface IRedmineService
    {
        Task<List<SyncUser>> GetAllUsersAsync();
    }
}