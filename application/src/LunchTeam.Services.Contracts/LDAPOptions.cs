﻿namespace LunchTeam.Services.Contracts
{
    public class LDAPOptions
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string NamingContext { get; set; }
        public bool AvatarsOnExchange { get; set; }
        public string AvatarUrlFormat { get; set; }
    }
}