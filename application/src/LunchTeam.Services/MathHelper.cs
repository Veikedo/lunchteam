﻿namespace LunchTeam.Services
{
    public static class MathHelper
    {
        /// <summary>
        ///     Bijects to unit interval
        /// </summary>
        /// <param name="max">max bound</param>
        /// <param name="value">the value to biject. Must be between min and max</param>
        /// <returns></returns>
        public static double UnitBijection(double max, double value)
        {
            return 1 / max * value;
        }
    }
}