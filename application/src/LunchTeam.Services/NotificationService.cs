﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using LunchTeam.Common;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;
using Microsoft.Extensions.Options;

namespace LunchTeam.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly SmtpOptions _smtpOptions;

        public NotificationService(IEmailTemplateService emailTemplateService, IOptions<SmtpOptions> options)
        {
            _emailTemplateService = emailTemplateService;
            _smtpOptions = options.Value;
        }

        public async Task NotifyContestCompletedAsync(IReadOnlyList<UserContestInfo> winners)
        {
            var winnerEmails = winners.Select(m => m.Email).ToList();
            var allEmails = new List<string> {_smtpOptions.AllAddress};

            if (DevUtils.IsDevelopment())
            {
                winnerEmails = allEmails = new List<string> {_smtpOptions.DevAddress};
            }

            string contestCompletedTemplate = await _emailTemplateService.ContestCompletedTemplateAsync(winners).ConfigureAwait(false);
            string forContestWinnersTemplate = await _emailTemplateService.ForContestWinnersTemplateAsync(winners).ConfigureAwait(false);

            await SendEmailsAsync(allEmails, "Едоки этой пятницы", contestCompletedTemplate)
                .ConfigureAwait(false);

            await SendEmailsAsync(winnerEmails, "Поздравляем! Ты в команде!", forContestWinnersTemplate)
                .ConfigureAwait(false);
        }

        public async Task NotifyNewContestStartedAsync(DateTime dinnerDate)
        {
            var emails = new List<string> {_smtpOptions.AllAddress};

            if (DevUtils.IsDevelopment())
            {
                emails = new List<string> {_smtpOptions.DevAddress};
            }

            string body = await _emailTemplateService
                .NewContestTemplateAsync(dinnerDate)
                .ConfigureAwait(false);

            await SendEmailsAsync(emails, "LunchTeam: новый спринт", body)
                .ConfigureAwait(false);
        }

        private Task SendEmailsAsync(IEnumerable<string> emails, string subject, string body)
        {
            var client = new SmtpClient {
                Host = _smtpOptions.Host,
                Credentials = new NetworkCredential(_smtpOptions.Login, _smtpOptions.Password, _smtpOptions.Domain),
                DeliveryMethod = SmtpDeliveryMethod.Network,
            };

            var message = new MailMessage(_smtpOptions.From, string.Join(",", emails)) {
                BodyEncoding = Encoding.UTF8,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                IsBodyHtml = true,
                Subject = subject,
                From = new MailAddress(_smtpOptions.From, "Lunch Team"),
                Body = body,
                CC = {_smtpOptions.DevAddress}
            };

            return client.SendMailAsync(message);
        }
    }
}