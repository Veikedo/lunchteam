﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Redmine.Net.Api;
using Redmine.Net.Api.Async;

namespace LunchTeam.Services.Redmine
{
    public static class RedmineManagerExtensions
    {
        private const int MaxApiLimit = 100;

        public static async Task<List<T>> GetObjectsAsync<T>(this RedmineManager redmineManager, NameValueCollection parameters)
            where T : class, new()
        {
            var retValue = new List<T>();

            var offset = 0;
            var query = new NameValueCollection(parameters) {
                ["limit"] = MaxApiLimit.ToString()
            };

            while (true)
            {
                var response = await redmineManager
                    .GetPaginatedObjectsAsync<T>(query)
                    .ConfigureAwait(false);

                retValue.AddRange(response.Objects);

                offset += MaxApiLimit;
                query["offset"] = offset.ToString();

                if (retValue.Count >= response.TotalCount)
                {
                    break;
                }
            }

            return retValue;
        }
    }
}