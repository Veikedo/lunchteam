﻿using System;
using AutoMapper;
using LunchTeam.Services.Contracts;
using Redmine.Net.Api.Types;

namespace LunchTeam.Services.Redmine
{
    public class RedmineMapperProfile : Profile
    {
        public RedmineMapperProfile()
        {
            CreateMap<User, SyncUser>()
                .ForMember(d => d.RegistrationDate, o => o.MapFrom(s => s.CreatedOn ?? DateTime.MinValue))
                ;
        }
    }
}