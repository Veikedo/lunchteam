﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using AutoMapper;
using LunchTeam.Core;
using LunchTeam.Dal.Extensions;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;
using Microsoft.Extensions.Options;
using Redmine.Net.Api;
using Redmine.Net.Api.Async;
using Redmine.Net.Api.Types;
using User = Redmine.Net.Api.Types.User;

namespace LunchTeam.Services.Redmine
{
    public class RedmineService : IRedmineService
    {
        private readonly IConfiguration _angleSharpOptions;
        private readonly IMapper _mapper;
        private readonly RedmineManager _redmineManager;
        private readonly RedmineOptions _redmineOptions;
        private bool _isInitialized;

        public RedmineService(IMapper mapper, IOptions<RedmineOptions> options)
        {
            _mapper = mapper;
            _redmineOptions = options.Value;
            _redmineManager = new RedmineManager(_redmineOptions.Host, _redmineOptions.ApiKey);
            _angleSharpOptions = Configuration.Default.WithDefaultLoader().WithCookies();
        }

        public async Task<List<SyncUser>> GetAllUsersAsync()
        {
            await InitializeAsync().ConfigureAwait(false);

            var rawUsers = await _redmineManager
                .GetObjectsAsync<User>(new NameValueCollection {["group_id"] = _redmineOptions.PlayersGroupId})
                .ConfigureAwait(false);

            var usersWithAvatars = await GetUserAvatarsAsync(rawUsers).ConfigureAwait(false);

            var girlsIds = await _redmineManager
                .GetObjectAsync<Group>(_redmineOptions.GirlsGroupId, new NameValueCollection {["include"] = "users"})
                .ContinueWith(t => t.Result.Users.Select(u => u.Id).ToHashSet())
                .ConfigureAwait(false);

            var result = new List<SyncUser>();
            foreach (var userWithAvatar in usersWithAvatars)
            {
                var user = userWithAvatar.Key;
                var avatar = userWithAvatar.Value;

                var redmineUser = _mapper.Map<SyncUser>(user);

                redmineUser.Sex = girlsIds.Contains(user.Id) ? Sex.Female : Sex.Male;
                redmineUser.JobPosition = await GetUserJobPositionAsync(user.Id).ConfigureAwait(false);
                redmineUser.Avatar = avatar;

                result.Add(redmineUser);
            }

            return result;
        }

        private async Task<Dictionary<User, byte[]>> GetUserAvatarsAsync(IEnumerable<User> users)
        {
            const string selector = "#content > div.person.details > table > tbody > tr > td.avatar > a > img";

            var result = new Dictionary<User, byte[]>();
            var ctx = BrowsingContext.New(_angleSharpOptions);
            var httpClient = await CreateHttpClientAsync(ctx).ConfigureAwait(false);

            foreach (var user in users)
            {
                var document = await ctx
                    .OpenAsync($"{_redmineOptions.Host}people/{user.Id}")
                    .ConfigureAwait(false);

                string avatarUrl = document.QuerySelector(selector)?.GetAttribute("src") ?? string.Empty;
                bool userHasRealAvatar = avatarUrl.Contains(_redmineOptions.Host);

                if (userHasRealAvatar)
                {
                    // the user does have avatar. Not just gravatar dumb pic
                    result[user] = await httpClient
                        .GetByteArrayAsync(avatarUrl)
                        .ConfigureAwait(false);
                }
                else
                {
                    result[user] = null;
                }
            }

            return result;
        }

        private async Task<HttpClient> CreateHttpClientAsync(IBrowsingContext ctx)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            var document = await ctx
                .OpenAsync($"{_redmineOptions.Host}people")
                .ConfigureAwait(false);

            string domain = _redmineOptions.Host.Replace("https://", string.Empty).Replace("/", string.Empty);
            string cookieName = document.Cookie.Replace("_redmine_session=", string.Empty);
            var handler = new HttpClientHandler {UseCookies = true};
            handler.CookieContainer.Add(new Cookie("_redmine_session", cookieName, "/", domain));

            return new HttpClient(handler);
        }

        private async Task InitializeAsync()
        {
            if (!_isInitialized)
            {
                await LoginToRedmineAsync().ConfigureAwait(false);
            }

            _isInitialized = true;
        }

        private async Task LoginToRedmineAsync()
        {
            var ctx = await BrowsingContext
                .New(_angleSharpOptions)
                .OpenAsync($"{_redmineOptions.Host}login/")
                .ConfigureAwait(false);

            await ctx
                .QuerySelector<IHtmlFormElement>("#login-form > form")
                .SubmitAsync(new {username = _redmineOptions.Login, password = _redmineOptions.Password})
                .ConfigureAwait(false);
        }

        private async Task<string> GetUserJobPositionAsync(int redmineUserId)
        {
            const string selector = "#attributes > table > tbody > tr > td.job_title.title";
            var document = await BrowsingContext
                .New(_angleSharpOptions)
                .OpenAsync($"{_redmineOptions.Host}people/{redmineUserId}")
                .ConfigureAwait(false);

            return document.QuerySelector(selector)?.Text() ?? "somebody";
        }
    }
}