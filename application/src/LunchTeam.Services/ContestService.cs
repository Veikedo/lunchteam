﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LunchTeam.Common;
using LunchTeam.Core;
using LunchTeam.Dal;
using LunchTeam.Dal.Extensions;
using LunchTeam.Domain;
using LunchTeam.Domain.Events;
using LunchTeam.Services.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LunchTeam.Services
{
    // todo candidate to refactor
    public class ContestService : IContestService
    {
        private readonly LunchTeamDb _db;
        private readonly IIdentityProvider _identityProvider;
        private readonly IMediator _mediator;
        private readonly Random _rnd;
        private readonly ScoreWeightsOptions _scoreWeights;

        public ContestService(LunchTeamDb db, IMediator mediator, IIdentityProvider identityProvider, IOptions<ScoreWeightsOptions> options)
        {
            _db = db;
            _mediator = mediator;
            _identityProvider = identityProvider;
            _rnd = new Random();
            _scoreWeights = options.Value;
        }

        public async Task CompleteCurrentContestAsync()
        {
            var contest = await _db.Contests.GetCurrentContestAsync().ConfigureAwait(false);
            var winners = await ChooseWinnersAsync(contest.Id).ConfigureAwait(false);

            var message = new ContestCompleted(contest.Id, winners);
            await _mediator.Publish(message).ConfigureAwait(false);
        }

        public async Task AddUserToCurrentContestAsync()
        {
            string userLogin = _identityProvider.GetCurrentUserLogin();
            var contestId = await _db.Contests.GetCurrentContestIdAsync().ConfigureAwait(false);

            bool isAlreadyJoined = await _db.ContestSagas
                .IsUserJoinedContest(userLogin, contestId)
                .ConfigureAwait(false);

            if (!isAlreadyJoined)
            {
                var questions = await GenerateQuestionsAsync(userLogin).ConfigureAwait(false);
                var contestSaga = new ContestSaga(userLogin, contestId, questions);

                _db.Add(contestSaga);
                await _db.SaveChangesAsync().ConfigureAwait(false);

                var message = new UserJoinedContest(userLogin);
                await _mediator.Publish(message).ConfigureAwait(false);
            }
        }

        private async Task<List<UserContestInfo>> ChooseWinnersAsync(Guid contestId)
        {
            var scores = await GetUserContestInfosAsync(contestId)
                .ContinueWith(t => t.Result.ToHashSet())
                .ConfigureAwait(false);

            var winners = new List<UserContestInfo>();
            double minScores = scores.Min(s => s.ContestScore);

            var isAutoChosen = false;
            int count = Math.Min(Constants.WinnersCount, scores.Count);
            for (var i = 0; i < count; i++)
            {
                var workSet = scores;
                if (!isAutoChosen)
                {
                    var autos = scores.Where(s => s.HasAuto).ToHashSet();
                    if (autos.Count > 0)
                    {
                        workSet = autos;
                    }

                    isAutoChosen = true;
                }

                double maxScores = workSet.Max(s => s.ContestScore);
                double filterBound = _rnd.NextDouble() * (maxScores - minScores) + minScores;

                var finalists = workSet.Where(s => s.ContestScore >= filterBound).ToList();
                var winner = finalists[_rnd.Next(0, finalists.Count)];

                winners.Add(winner);
                scores.Remove(winner);
            }

            var driver = winners.Where(u => u.HasAuto).GetRandomItem();
            if (driver != null)
            {
                driver.IsDriver = true;
            }

            return winners;
        }

        private async Task<List<UserContestInfo>> GetUserContestInfosAsync(Guid contestId)
        {
            var relax = TimeSpan.FromDays(14);
            var today = DateTime.Today;

            // splitted to 2 queries because of bug in EF7
            var usersWithLastWinDateQuery =
                from user in _db.Users
                let winDates = _db.ContestSagas
                    .Where(s => s.UserLogin == user.Login && s.IsWinner)
                    .Select(s => s.Contest.DinnerDate)
                    .OrderByDescending(d => d)
                select new {
                    User = user,
                    LastWinDate = winDates.FirstOrDefault(),
                };

            var usersWithLastWinDate = await usersWithLastWinDateQuery
                .ToListAsync()
                .ConfigureAwait(false);

            var query = from userWithLastWinDate in usersWithLastWinDate
                        let user = userWithLastWinDate.User
                        let lastWinDate = userWithLastWinDate.LastWinDate
                        join saga in _db.ContestSagas.Include(x => x.Questions) on user.Login equals saga.UserLogin
                        let correctAnswers = saga.Questions.Count(q => q.IsAnswerCorrect)
                        where saga.ContestId == contestId
                              && correctAnswers > 0
                              && today - lastWinDate >= relax
                              && user.IsActive
                        select new UserContestInfo {
                            UserLogin = user.Login,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            JobPosition = user.JobPosition,
                            Email = user.Email,
                            RegistrationDate = user.RegistrationDate ?? DateTime.Today,
                            CorrectAnswers = correctAnswers,
                            LastWinDate = lastWinDate,
                            HasAuto = saga.HasAuto,
                        };

            return CountContestScore(query.ToList());
        }

        private List<UserContestInfo> CountContestScore(List<UserContestInfo> infos)
        {
            double maxDaysFromReg = double.MinValue;
            double maxDaysFromWin = double.MinValue;
            double maxCorrectAnswers = double.MinValue;

            foreach (var info in infos)
            {
                maxDaysFromReg = Math.Max(info.DaysFromRegistration, maxDaysFromReg);
                maxDaysFromWin = Math.Max(info.DaysFromWin, maxDaysFromWin);
                maxCorrectAnswers = Math.Max(info.CorrectAnswers, maxCorrectAnswers);
            }

            const double powBase = Math.E;

            foreach (var info in infos)
            {
                double fromRegPow = MathHelper.UnitBijection(maxDaysFromReg, 1 / info.DaysFromRegistration);
                info.ContestScore += Math.Pow(powBase * _scoreWeights.Registration, fromRegPow);

                double fromWinPow = MathHelper.UnitBijection(maxDaysFromWin, info.DaysFromWin);
                info.ContestScore += Math.Pow(powBase * _scoreWeights.Victory, fromWinPow);

                double correctAnswersPow = MathHelper.UnitBijection(maxCorrectAnswers, info.CorrectAnswers);
                info.ContestScore += Math.Pow(powBase * _scoreWeights.Answers, correctAnswersPow);
            }

            return infos;
        }

        private async Task<List<Question>> GenerateQuestionsAsync(string userLogin)
        {
            const int variantsCount = Constants.QuestionsCount - 1;

            var users = await _db.Users
                .Where(user => user.Login != userLogin && user.CanBeHidden)
                .Select(u => new {u.Login, u.Sex})
                .ToHashSetAsync()
                .ConfigureAwait(false);

            var hiddenUsers = users.GetRandomItems(Constants.QuestionsCount);
            users.RemoveRange(hiddenUsers);

            var sexLookup = users.ToLookup(u => u.Sex, u => u);

            var questions = from hiddenUser in hiddenUsers
                            let correctVariant = new Variant(hiddenUser.Login)
                            let variants = sexLookup[hiddenUser.Sex]
                                .GetRandomItems(variantsCount)
                                .Select(u => new Variant(u.Login))
                                .Concat(correctVariant)
                                .Shuffle()
                            select new Question(variants, correctVariant.Id);

            return questions.ToList();
        }
    }
}