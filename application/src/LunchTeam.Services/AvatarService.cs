﻿using System.IO;
using System.Threading.Tasks;
using LunchTeam.Common;
using LunchTeam.Core;
using LunchTeam.Services.Contracts;
using Microsoft.Extensions.Options;
using PhotoSauce.MagicScaler;
using UserAvatar = LunchTeam.Domain.Avatar;

namespace LunchTeam.Services
{
    public class AvatarService : IAvatarService
    {
        private readonly string _croppedAvatarsPath;
        private readonly string _notCroppedAvatarsPath;

        public AvatarService(IOptions<AppOptions> options)
        {
            var appOptions = options.Value;
            _croppedAvatarsPath = $"{appOptions.AvatarFolder}Cropped\\";
            _notCroppedAvatarsPath = $"{appOptions.AvatarFolder}NotCropped\\";
        }

        public async Task<UserAvatar> GetUserAvatarAsync(string userLogin)
        {
            string cropped = _croppedAvatarsPath + $"{userLogin}.png";
            string notCropped = _notCroppedAvatarsPath + $"{userLogin}.png";

            if (File.Exists(cropped))
            {
                var bytes = await FileUtils.ReadAllFileAsync(cropped).ConfigureAwait(false);
                return new UserAvatar(userLogin, bytes, true);
            }

            if (File.Exists(notCropped))
            {
                var bytes = await FileUtils.ReadAllFileAsync(notCropped).ConfigureAwait(false);
                return new UserAvatar(userLogin, bytes, false);
            }

            throw new FileNotFoundException("File not found", userLogin);
        }

        public bool DoesUserHaveAvatar(string userLogin)
        {
            string cropped = _croppedAvatarsPath + $"{userLogin}.png";
            string notCropped = _notCroppedAvatarsPath + $"{userLogin}.png";

            return File.Exists(cropped) || File.Exists(notCropped);
        }

        public void SaveUserAvatar(UserAvatar avatar)
        {
            string directory = avatar.IsCropped ? _croppedAvatarsPath : _notCroppedAvatarsPath;

            Directory.CreateDirectory(directory);
            File.WriteAllBytes(directory + $"{avatar.UserLogin}.png", avatar.Image);
        }
    }
}