﻿using System.Collections.Generic;
using LinqToLdap.Mapping;
using LunchTeam.Services.Contracts;

namespace LunchTeam.Services.LDAP
{
    internal class SyncUserLDAPMapping : ClassMap<SyncUser>
    {
        public override IClassMap PerformMapping(string namingContext = null,
            string objectCategory = null, bool includeObjectCategory = true,
            IEnumerable<string> objectClasses = null, bool includeObjectClasses = true)
        {
            ObjectCategory("Person");
            ObjectClass("User");

            Map(x => x.Login).Named("sAMAccountName").ReadOnly();
            Map(x => x.FirstName).Named("givenName").ReadOnly();
            Map(x => x.LastName).Named("sn").ReadOnly();
            Map(x => x.JobPosition).Named("title").ReadOnly();
            Map(x => x.Email).Named("mail").ReadOnly();

            // AD avatars are too small
            // Map(x => x.Avatar).Named("thumbnailPhoto").ReadOnly();

            return this;
        }
    }
}