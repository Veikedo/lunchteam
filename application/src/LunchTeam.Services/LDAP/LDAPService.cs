﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using LinqToLdap;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;
using Microsoft.Extensions.Options;
using LunchTeam.Genderize;

namespace LunchTeam.Services.LDAP
{
    public class LDAPService : ILDAPService
    {
        private readonly List<LDAPOptions> _ldapOptions;

        public LDAPService(IOptions<List<LDAPOptions>> options)
        {
            _ldapOptions = options.Value;
        }

        public Task<List<SyncUser>> GetAllUsersAsync()
        {
            var tasks = _ldapOptions.Select(GetLDAPUsersAsync);
            return Task
                .WhenAll(tasks)
                .ContinueWith(t => t.Result.SelectMany(x => x).ToList());
        }

        private static async Task<IEnumerable<SyncUser>> GetLDAPUsersAsync(LDAPOptions options)
        {
            var config = new LdapConfiguration().MaxPageSizeIs(1000).AddMapping(new SyncUserLDAPMapping());
            var factory = new IgnoreServerErrorsConnectionFactory(options);
            config.ConfigureCustomFactory(factory);

            var context = new DirectoryContext(config);
            var users = context
                .Query<SyncUser>(namingContext: options.NamingContext)
                .AsEnumerable()
                .Where(x => x.IsHuman)
                .ToList()
                ;

            await GenderizeUsersAsync(users).ConfigureAwait(false);
            if (options.AvatarsOnExchange)
            {
                await DownloadUserAvatarsAsync(users, options).ConfigureAwait(false);
            }

            return users;
        }

        private static async Task GenderizeUsersAsync(List<SyncUser> users)
        {
            var client = new GenderizeClient();
            var lookup = users
                .Where(x => x.Sex == null)
                .ToLookup(x => x.FirstName, x => x);

            try
            {
                var genders = await client
                    .GenderizeMultipleNamesAsync(lookup.Select(x => x.Key))
                    .ContinueWith(t => t.Result.Where(x => x.Gender != null && x.Probability > 0.7))
                    .ConfigureAwait(false);

                foreach (var gender in genders)
                {
                    foreach (var user in lookup[gender.Name])
                    {
                        user.Sex = gender.Gender == Gender.Male ? Sex.Male : Sex.Female;
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private static async Task DownloadUserAvatarsAsync(List<SyncUser> users, LDAPOptions options)
        {
            var httpClient = CreateHttpClient(options);
            string avatarUrlFormat = options.AvatarUrlFormat;

            async Task DownloadUserAvatar(SyncUser user)
            {
                try
                {
                    string avatarUrl = string.Format(avatarUrlFormat, user.Login);
                    user.Avatar = await httpClient.GetByteArrayAsync(avatarUrl).ConfigureAwait(false);
                }
                catch (Exception)
                {
                    user.Avatar = null;
                }
            }

            var tasks = users.Select(DownloadUserAvatar);
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }

        private static HttpClient CreateHttpClient(LDAPOptions options)
        {
            var handler = new HttpClientHandler {
                Credentials = new NetworkCredential(options.Login, options.Password)
            };

            return new HttpClient(handler);
        }
    }
}