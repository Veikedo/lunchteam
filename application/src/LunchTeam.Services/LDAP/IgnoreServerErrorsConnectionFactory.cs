﻿using System.DirectoryServices.Protocols;
using System.Net;
using LinqToLdap;
using LunchTeam.Services.Contracts;

namespace LunchTeam.Services.LDAP
{
    public class IgnoreServerErrorsConnectionFactory : ILdapConnectionFactory
    {
        private readonly string _host;
        private readonly string _login;
        private readonly string _password;
        private readonly int _port;

        public IgnoreServerErrorsConnectionFactory(LDAPOptions options)
        {
            _host = options.Host;
            _login = options.Login;
            _password = options.Password;
            _port = options.Port;
        }

        public LdapConnection GetConnection()
        {
            return new LdapConnection(new LdapDirectoryIdentifier(_host, _port)) {
                Credential = new NetworkCredential(_login, _password),
                SessionOptions = {
                    SecureSocketLayer = true,
                    ProtocolVersion = 3,
                    VerifyServerCertificate = (connection, certificate) => true,
                },
            };
        }

        public void ReleaseConnection(LdapConnection connection)
        {
            connection.Dispose();
        }
    }
}