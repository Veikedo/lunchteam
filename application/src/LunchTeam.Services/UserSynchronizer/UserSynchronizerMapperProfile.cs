﻿using AutoMapper;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;

namespace LunchTeam.Services.UserSynchronizer
{
    public class UserSynchronizerMapperProfile : Profile
    {
        public UserSynchronizerMapperProfile()
        {
            CreateMap<SyncUser, User>();
            CreateMap<User, SyncUser>();
        }
    }
}