using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using LunchTeam.Core;
using LunchTeam.Dal;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace LunchTeam.Services.UserSynchronizer
{
    public class UserSynchronizer : IUserSynchronizer
    {
        private readonly IAvatarService _avatarService;
        private readonly LunchTeamDb _db;
        private readonly ILDAPService _ldapService;
        private readonly IMapper _mapper;
        private readonly IRedmineService _redmineService;

        public UserSynchronizer(IRedmineService redmineService, LunchTeamDb db, IMapper mapper, IAvatarService avatarService,
            ILDAPService ldapService)
        {
            _redmineService = redmineService;
            _db = db;
            _mapper = mapper;
            _avatarService = avatarService;
            _ldapService = ldapService;
        }

        public async Task SynchronizeAsync()
        {
            var existedUsers = await _db.Users.ProjectTo<SyncUser>().ToListAsync().ConfigureAwait(false);
            var syncUsers = await GetSyncUsersAsync(existedUsers).ConfigureAwait(false);
            var lunchTeamUsers = new List<User>();

            foreach (var syncUser in syncUsers)
            {
                var user = _mapper.Map<User>(syncUser);
                lunchTeamUsers.Add(user);

                if (syncUser.Avatar != null)
                {
                    var avatar = new Avatar(syncUser.Login, syncUser.Avatar, isCropped: false);
                    _avatarService.SaveUserAvatar(avatar);

                    user.Activate();
                }
                else if (_avatarService.DoesUserHaveAvatar(syncUser.Login))
                {
                    user.Activate();
                }
            }

            await _db.Users.SynchronizeAsync(lunchTeamUsers).ConfigureAwait(false);
            await _db.SaveChangesAsync().ConfigureAwait(false);
        }

        private async Task<List<SyncUser>> GetSyncUsersAsync(List<SyncUser> existedUsers)
        {
            var redmineUsers = await _redmineService.GetAllUsersAsync().ConfigureAwait(false);
            var ldapUsers = await _ldapService.GetAllUsersAsync().ConfigureAwait(false);

            var syncUsers = redmineUsers
                .Concat(ldapUsers)
                .Concat(existedUsers)
                .GroupBy(x => x.Login, x => x, StringComparer.OrdinalIgnoreCase)
                .Select(g => new SyncUser {
                    Login = g.Key,
                    Avatar = g.FirstNotNull(x => x.Avatar),
                    Email = g.FirstNotNull(x => x.Email),
                    JobPosition = g.FirstNotNull(x => x.JobPosition),
                    FirstName = g.FirstNotNull(x => x.FirstName),
                    LastName = g.FirstNotNull(x => x.LastName),
                    RegistrationDate = g.FirstNotNull(x => x.RegistrationDate),
                    Sex = g.FirstNotNull(x => x.Sex),
                })
                .ToList();

            return syncUsers;
        }
    }
}