﻿namespace LunchTeam.Core
{
    public static class Constants
    {
        public const int QuestionsCount = 3;
        public const int WinnersCount = 4;
    }
}