﻿namespace LunchTeam.Core
{
    public interface IIdentityProvider
    {
        string GetCurrentUserLogin();
    }
}