﻿using System.Collections.Generic;
using MediatR;

namespace LunchTeam.Core
{
    public interface IHasEvents
    {
        IEnumerable<INotification> Events { get; }
    }
}