﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchTeam.Core
{
    public static class CollectionExtensions
    {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }

        public static int RemoveRange<T>(this HashSet<T> source, params T[] items)
        {
            return items.Count(source.Remove);
        }

        public static int RemoveRange<T>(this HashSet<T> source, IEnumerable<T> items)
        {
            return items.Count(source.Remove);
        }

        public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, params TSource[] second)
        {
            return first.Concat(second.AsEnumerable());
        }

        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int batchCount)
        {
            while (source.Any())
            {
                yield return source.Take(batchCount);
                source = source.Skip(batchCount);
            }
        }

        public static TDestination FirstNotNull<TSource, TDestination>(this IEnumerable<TSource> first,
            Func<TSource, TDestination> selector)
        {
            return first.Select(selector).FirstOrDefault(s => s != null);
        }

        public static IEnumerable<TSource> Do<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            foreach (var item in source)
            {
                action(item);
                yield return item;
            }
        }

        public static IEnumerable<TSource> Do<TSource>(this IEnumerable<TSource> source, Action<TSource, int> action)
        {
            var idx = 0;
            foreach (var item in source)
            {
                action(item, idx);
                idx++;

                yield return item;
            }
        }
    }
}