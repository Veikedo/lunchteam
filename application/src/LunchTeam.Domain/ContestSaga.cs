using System;
using System.Collections.Generic;
using System.Linq;
using LunchTeam.Common;
using LunchTeam.Core;
using LunchTeam.Domain.Events;

namespace LunchTeam.Domain
{
    public class ContestSaga
    {
        public ContestSaga(string userLogin, Guid contestId, IEnumerable<Question> questions)
        {
            Id = Guid.NewGuid();
            State = States.Started;
            UserLogin = userLogin;
            ContestId = contestId;
            Questions = questions.ToList();
        }

        [Obsolete(ErrorStrings.ForBindersOnly, true)]
        private ContestSaga()
        {
        }

        public enum States
        {
            NotStarted = 0,
            Started = 1,
            Completed = 2,
            Lost = 3,
            Won = 4,
        }

        public Guid Id { get; private set; }
        public string UserLogin { get; private set; }
        public Guid ContestId { get; private set; }
        public Contest Contest { get; private set; }
        public List<Question> Questions { get; private set; }
        public bool IsWinner => State == States.Won;
        public bool IsDriver { get; private set; }
        public bool HasAuto { get; set; }
        public States State { get; private set; }

        public void Handle(QuestionAnswered message)
        {
            if (Questions.Count(q => q.IsAnswered) >= Constants.QuestionsCount)
            {
                State = Questions.All(q => !q.IsAnswerCorrect) ? States.Lost : States.Completed;
            }
        }

        public void Complete(IEnumerable<UserContestInfo> winners)
        {
            var myUser = winners.FirstOrDefault(w => w.UserLogin == UserLogin);
            if (myUser != null)
            {
                State = States.Won;
                IsDriver = myUser.IsDriver;
            }
            else
            {
                State = States.Lost;
            }
        }
    }
}