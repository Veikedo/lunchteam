﻿namespace LunchTeam.Domain
{
    public class Avatar
    {
        public Avatar(string userLogin, byte[] image, bool isCropped)
        {
            UserLogin = userLogin;
            IsCropped = isCropped;
            Image = image;
        }

        public string UserLogin { get; private set; }
        public bool IsCropped { get; private set; }
        public byte[] Image { get; private set; }
    }
}