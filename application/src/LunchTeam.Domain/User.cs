﻿using System;
using LunchTeam.Common;

namespace LunchTeam.Domain
{
    public class User
    {
        public User(string login)
        {
            Login = login;
            RegistrationDate = DateTime.Now;
        }

        [Obsolete(ErrorStrings.ForBindersOnly, true)]
        private User()
        {
        }

        public string Login { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string JobPosition { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Email { get; set; }
        public Sex? Sex { get; set; }
        public bool IsActive { get; private set; }
        public bool CanBeHidden => IsActive && Sex != null;

        public void Activate()
        {
            IsActive = true;
        }

        public void Deactivate()
        {
            IsActive = false;
        }
    }
}