using System;

namespace LunchTeam.Domain
{
    public class AnswerResult
    {
        internal AnswerResult(bool isAnswerCorrect, Guid? correctVariantId = null)
        {
            IsAnswerCorrect = isAnswerCorrect;
            CorrectVariantId = correctVariantId;
        }

        public bool IsAnswerCorrect { get; }
        public Guid? CorrectVariantId { get; }

        public static AnswerResult Failed(Guid? correctVariantId = null)
        {
            return new AnswerResult(false, correctVariantId);
        }

        public static AnswerResult Passed(Guid? correctVariantId)
        {
            return new AnswerResult(true, correctVariantId);
        }
    }
}