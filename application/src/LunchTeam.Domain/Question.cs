using System;
using System.Collections.Generic;
using LunchTeam.Common;
using LunchTeam.Core;
using LunchTeam.Domain.Events;
using MediatR;

namespace LunchTeam.Domain
{
    public class Question : IHasEvents
    {
        private readonly List<INotification> _events = new List<INotification>();

        public Question(IReadOnlyList<Variant> variants, Guid correctVariantId)
        {
            Id = Guid.NewGuid();
            Variants = variants;
            CorrectVariantId = correctVariantId;
        }

        [Obsolete(ErrorStrings.ForBindersOnly, true)]
        public Question()
        {
        }

        public Guid Id { get; private set; }
        public IReadOnlyList<Variant> Variants { get; private set; }
        public Guid CorrectVariantId { get; private set; }
        public Guid? AnsweredVariantId { get; private set; }    
        public bool IsAnswerCorrect => CorrectVariantId == AnsweredVariantId;
        public bool IsAnswered => AnsweredVariantId != null;
        public Guid ContestSagaId { get; private set; }
        public IEnumerable<INotification> Events => _events;

        public AnswerResult Answer(Guid variantId)
        {
            if (IsAnswered)
            {
                return AnswerResult.Failed();
            }

            AnsweredVariantId = variantId;
            _events.Add(new QuestionAnswered(Id, ContestSagaId));

            return new AnswerResult(IsAnswerCorrect, CorrectVariantId);
        }
    }
}