using System;

namespace LunchTeam.Domain
{
    public class UserContestInfo
    {
        public bool HasAuto { get; set; }
        public DateTime RegistrationDate { get; set; }
        public double DaysFromRegistration => (DateTime.Today - RegistrationDate).TotalDays;
        public DateTime LastWinDate { get; set; }
        public double DaysFromWin => (DateTime.Today - LastWinDate).TotalDays;
        public bool IsDriver { get; set; }
        public int CorrectAnswers { get; set; }
        public double ContestScore { get; set; }
        public string UserLogin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string JobPosition { get; set; }
        public string Email { get; set; }
    }
}