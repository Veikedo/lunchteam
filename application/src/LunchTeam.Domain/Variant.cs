using System;
using LunchTeam.Common;

namespace LunchTeam.Domain
{
    public class Variant
    {
        public Variant(string hiddenUserLogin)
        {
            Id = Guid.NewGuid();
            HiddenUserLogin = hiddenUserLogin;
        }

        [Obsolete(ErrorStrings.ForBindersOnly, true)]
        private Variant()
        {
        }

        public Guid Id { get; private set; }
        public string HiddenUserLogin { get; private set; }
        public User HiddenUser { get; private set; }
    }
}