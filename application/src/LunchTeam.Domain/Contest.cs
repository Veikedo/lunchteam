﻿using System;
using LunchTeam.Common;

namespace LunchTeam.Domain
{
    public class Contest
    {
        public Contest(DateTime dinnerDate)
        {
            Id = Guid.NewGuid();
            DinnerDate = dinnerDate;
        }

        [Obsolete(ErrorStrings.ForBindersOnly, true)]
        private Contest()
        {
        }

        public Guid Id { get; private set; }
        public DateTime DinnerDate { get; private set; }
        public bool IsCompleted { get; private set; }

        public void Complete()
        {
            if (IsCompleted)
            {
                throw new InvalidOperationException("Already completed");
            }

            IsCompleted = true;
        }
    }
}