﻿namespace LunchTeam.Domain
{
    public enum Sex
    {
        Male = 0,
        Female = 1
    }
}