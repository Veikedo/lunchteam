﻿using MediatR;

namespace LunchTeam.Domain.Events
{
    public class NewContestAdded : INotification
    {
        public NewContestAdded(Contest contest)
        {
            Contest = contest;
        }

        public Contest Contest { get; }
    }
}