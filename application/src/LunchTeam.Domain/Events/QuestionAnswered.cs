﻿using System;
using MediatR;

namespace LunchTeam.Domain.Events
{
    public class QuestionAnswered : INotification
    {
        public QuestionAnswered(Guid questionId, Guid contestSagaId)
        {
            QuestionId = questionId;
            ContestSagaId = contestSagaId;
        }

        public Guid QuestionId { get; }
        public Guid ContestSagaId { get; }
    }
}