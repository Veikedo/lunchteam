﻿using System;
using System.Collections.Generic;
using MediatR;

namespace LunchTeam.Domain.Events
{
    public class ContestCompleted : INotification
    {
        public ContestCompleted(Guid contestId, IReadOnlyList<UserContestInfo> winners)
        {
            ContestId = contestId;
            Winners = winners;
        }

        public Guid ContestId { get; }
        public IReadOnlyList<UserContestInfo> Winners { get; }
    }
}