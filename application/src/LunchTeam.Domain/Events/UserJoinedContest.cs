﻿using MediatR;

namespace LunchTeam.Domain.Events
{
    public class UserJoinedContest : INotification
    {
        public UserJoinedContest(string userLogin)
        {
            UserLogin = userLogin;
        }

        public string UserLogin { get; }
    }
}