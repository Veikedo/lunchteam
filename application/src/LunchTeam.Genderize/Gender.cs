namespace LunchTeam.Genderize
{
    public enum Gender
    {
        Male,
        Female,
        None,
    }
}