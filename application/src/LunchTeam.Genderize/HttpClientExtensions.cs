﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchTeam.Genderize
{
    public static class HttpClientExtensions
    {
        public static Task<TResult> DeserializeJsonAsync<TResult>(this HttpContent httpContent)
        {
            return httpContent
                .ReadAsStringAsync()
                .ContinueWith(t => JsonConvert.DeserializeObject<TResult>(t.Result));
        }
    }
}