using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LunchTeam.Core;

namespace LunchTeam.Genderize
{
    public class GenderizeClient
    {
        private HttpClient _client;
        private const string BaseApiUrl = "https://api.genderize.io/?country_id=ru&";
        private HttpClient HttpClient => _client ?? (_client = new HttpClient());

        public Task<GenderizeResponse> GenderizeSingleNameAsync(string name)
        {
            string url = $"{BaseApiUrl}name={name}";
            return MakeRequestAsync<GenderizeResponse>(url);
        }

        public Task<GenderizeResponse[]> GenderizeMultipleNamesAsync(IEnumerable<string> names)
        {
            const int maxNamesPerRequest = 10;
            var tasks = names
                    .Batch(maxNamesPerRequest)
                    .Select(batch => batch.Aggregate(BaseApiUrl, (aggr, current) => $"{aggr}name[]={current}&"))
                    .Select(MakeRequestAsync<GenderizeResponse[]>)
                ;

            return Task
                .WhenAll(tasks)
                .ContinueWith(t => t.Result.SelectMany(x => x).ToArray());
        }

        private async Task<T> MakeRequestAsync<T>(string url)
        {
            var response = await HttpClient.GetAsync(url).ConfigureAwait(false);
            return await response.Content
                .DeserializeJsonAsync<T>()
                .ConfigureAwait(false);
        }
    }
}