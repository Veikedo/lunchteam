using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LunchTeam.Genderize
{
    public class GenderizeResponse
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("gender")]
        public Gender? Gender { get; set; }

        [JsonProperty("probability")]
        public double Probability { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }
    }
}