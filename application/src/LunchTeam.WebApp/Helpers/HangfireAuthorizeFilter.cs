﻿using Hangfire.Dashboard;

namespace LunchTeam.WebApp.Helpers
{
    public class HangfireAuthorizeFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return context.GetHttpContext().User.IsInRole("Administrator");
        }
    }
}