﻿using LunchTeam.Core;
using Microsoft.AspNetCore.Http;

namespace LunchTeam.WebApp.Helpers
{
    internal class IdentityProvider : IIdentityProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IdentityProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private HttpContext HttpContext => _httpContextAccessor.HttpContext;

        public string GetCurrentUserLogin()
        {
            return HttpContext.User.Identity.Name;
        }
    }
}