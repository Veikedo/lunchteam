using System.Threading.Tasks;
using LunchTeam.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;

namespace LunchTeam.WebApp.Helpers
{
    public class ADUserAuthorizationHandler : IAuthorizationHandler
    {
        private readonly IMediator _mediator;

        public ADUserAuthorizationHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task HandleAsync(AuthorizationHandlerContext context)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                return _mediator.Send(new CreateUserIfNotExists());
            }

            return Task.CompletedTask;
        }
    }
}