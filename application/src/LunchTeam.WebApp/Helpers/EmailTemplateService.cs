﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LunchTeam.Domain;
using LunchTeam.Services.Contracts;

namespace LunchTeam.WebApp.Helpers
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly IViewRenderService _viewRenderService;

        public EmailTemplateService(IViewRenderService viewRenderService)
        {
            _viewRenderService = viewRenderService;
        }

        public async Task<string> NewContestTemplateAsync(DateTime dinnerDate)
        {
            string formattedDinnerDate = FormatDinnerDate(dinnerDate);
            return await _viewRenderService
                .RenderToStringAsync("Email/NewSprint", formattedDinnerDate)
                .ConfigureAwait(false);
        }

        public async Task<string> ContestCompletedTemplateAsync(IReadOnlyList<UserContestInfo> winners)
        {
            return await _viewRenderService
                .RenderToStringAsync("Email/ContestResult", winners)
                .ConfigureAwait(false);
        }

        public async Task<string> ForContestWinnersTemplateAsync(IReadOnlyList<UserContestInfo> winners)
        {
            return await _viewRenderService
                .RenderToStringAsync("Email/ContestWinnersCongrats", winners)
                .ConfigureAwait(false);
        }

        private static string FormatDinnerDate(DateTime dinnerDate)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("ru-ru");
            var sb = new StringBuilder();

            var dayOfWeek = dinnerDate.DayOfWeek;
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    sb.Append($"в {DateTimeFormatInfo.CurrentInfo?.GetDayName(dayOfWeek)}, ");
                    break;

                case DayOfWeek.Tuesday:
                    sb.Append("во вторник, ");
                    break;
                case DayOfWeek.Wednesday:
                    sb.Append("в среду, ");
                    break;

                case DayOfWeek.Friday:
                    sb.Append("в пятницу, ");
                    break;
            }

            sb.Append(dinnerDate.ToString("dd MMMM").ToLower());
            return sb.ToString();
        }
    }
}