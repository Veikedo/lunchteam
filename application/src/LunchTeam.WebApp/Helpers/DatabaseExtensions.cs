﻿using LunchTeam.Dal;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LunchTeam.WebApp.Helpers
{
    public static class DatabaseExtensions
    {
        public static void MigrateDatabase(this IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetService<LunchTeamDb>();
            context.Database.Migrate();
        }
    }
}