﻿using System;

namespace LunchTeam.WebApp.Helpers
{
    public class DomainCredentialsException : Exception
    {
        public DomainCredentialsException()
        {
        }

        public DomainCredentialsException(string message)
            : this(message, null)
        {
        }

        public DomainCredentialsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}