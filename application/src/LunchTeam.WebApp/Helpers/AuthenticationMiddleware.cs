﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace LunchTeam.WebApp.Helpers
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly RoleOptions _roleOptions;

        public AuthenticationMiddleware(RequestDelegate next, IOptions<RoleOptions> options)
        {
            _next = next;
            _roleOptions = options.Value;
        }

        public Task Invoke(HttpContext context)
        {
            var user = context.User;
            if (user.Identity.IsAuthenticated)
            {
                string userName = ActiveDirectoryHelper.GetUserName(user.Identity.Name);
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, userName)
                };

                if (_roleOptions.Administrators.Contains(userName))
                {
                    var claim = new Claim(ClaimTypes.Role, "Administrator");
                    claims.Add(claim);
                }

                var claimsIdentity = new ClaimsIdentity(claims, "ad-domain");
                context.User = new ClaimsPrincipal(claimsIdentity);
            }

            return _next(context);
        }
    }
}