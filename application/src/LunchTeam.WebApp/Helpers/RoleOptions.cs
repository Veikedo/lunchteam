﻿using System.Collections.Generic;

namespace LunchTeam.WebApp.Helpers
{
    public class RoleOptions
    {
        public List<string> Administrators { get; set; }
    }
}