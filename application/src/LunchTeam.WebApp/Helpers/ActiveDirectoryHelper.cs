﻿using System;
using System.Text.RegularExpressions;

namespace LunchTeam.WebApp.Helpers
{
    public static class ActiveDirectoryHelper
    {
        private static readonly Regex DomainUserRegex =
            new Regex("^(?<domain>[a-zA-Z][a-zA-Z0-9\\.-]+)\\\\(?<user>[^\\\\/\"\\[\\]:|<>\\+=;,\\?\\*@]+)$");

        private static readonly Regex UpnUserRegex =
            new Regex("^(?<user>[^\\\\/\"\\[\\]:|<>\\+=;,\\?\\*@]+)@(?<domain>[a-zA-Z][a-zA-Z0-9\\.-]+)$");

        public static string GetUserName(string userNameWithDomain)
        {
            string domain;
            string userName;
            Parse(userNameWithDomain, out domain, out userName);

            return userName;
        }

        private static void Parse(string userNameWithDomain, out string domainName, out string userName)
        {
            if (IsNameContainDomain(userNameWithDomain))
            {
                SplitUserNameText(userNameWithDomain, out domainName, out userName);
            }
            else
            {
                userName = userNameWithDomain;
                domainName = Environment.MachineName;
            }
        }

        private static bool IsNameContainDomain(string userName)
        {
            return UpnUserRegex.Match(userName).Success || DomainUserRegex.Match(userName).Success;
        }

        private static void SplitUserNameText(string userNameWithDomain, out string domainName, out string user)
        {
            var match = UpnUserRegex.Match(userNameWithDomain);
            if (!match.Success)
            {
                match = DomainUserRegex.Match(userNameWithDomain);
                if (!match.Success)
                {
                    throw new DomainCredentialsException(
                        "Invalid input parameter: userNameWithDomain. Probably, domain name is missed");
                }
            }
            domainName = match.Groups["domain"].Value;
            user = match.Groups["user"].Value;
        }
    }
}