﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Hangfire;
using LunchTeam.Commands;
using LunchTeam.Commands.EventHandlers;
using LunchTeam.Core;
using LunchTeam.Dal;
using LunchTeam.Queries.ContestInfo;
using LunchTeam.Queries.UserQuestions;
using LunchTeam.Services;
using LunchTeam.Services.Contracts;
using LunchTeam.Services.LDAP;
using LunchTeam.Services.Redmine;
using LunchTeam.Services.UserSynchronizer;
using LunchTeam.WebApp.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace LunchTeam.WebApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets("aspnet-LunchTeam.WebApp-20170110125929");
            }

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration["Data:DefaultConnection:ConnectionString"];
            services.AddDbContext<LunchTeamDb>(options => options.UseSqlServer(connection));

            services.AddHangfire(configuration => configuration.UseSqlServerStorage(connection));
            services.AddMvc();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IIdentityProvider, IdentityProvider>();
            services.AddScoped<IRedmineService, RedmineService>();
            services.AddScoped<ILDAPService, LDAPService>();
            services.AddScoped<IUserSynchronizer, UserSynchronizer>();
            services.AddScoped<IContestService, ContestService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IViewRenderService, ViewRenderService>();
            services.AddScoped<IAvatarService, AvatarService>();

            services.AddMediatR(typeof(ContestInfoQuery), typeof(AddNewContest));

            services.AddAutoMapper(typeof(UserQuestionMapperProfile), typeof(RedmineMapperProfile));

            services.AddAuthorization(o => o.AddPolicy("admin", p => p.RequireRole("Administrator")));
            services.AddScoped<IAuthorizationHandler, ADUserAuthorizationHandler>();
            services.AddScoped<IEmailTemplateService, EmailTemplateService>();

            services.TryAddTransient<NewContestAddedHandler>();

            services.Configure<RedmineOptions>(Configuration.GetSection("redmine"));
            services.Configure<RoleOptions>(Configuration.GetSection("role"));
            services.Configure<AppOptions>(Configuration.GetSection("app"));
            services.Configure<ScoreWeightsOptions>(Configuration.GetSection("scoreWeights"));
            services.Configure<SmtpOptions>(Configuration.GetSection("smtp"));
            services.Configure<List<LDAPOptions>>(Configuration.GetSection("ldap"));

            services.Configure<IISOptions>(opt => {
                opt.ForwardWindowsAuthentication = true;
                opt.AutomaticAuthentication = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseMiddleware<AuthenticationMiddleware>();

            app.MigrateDatabase();

            app.UseStaticFiles();

            app.UseHangfireDashboard(options: new DashboardOptions {
                Authorization = new[] {new HangfireAuthorizeFilter()}
            });
            app.UseHangfireServer();

            app.UseCors(o => o
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin()
                .AllowCredentials()
                .SetPreflightMaxAge(TimeSpan.FromSeconds(2520))
            );

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new {controller = "Home", action = "Index"});
            });

            RecurringJob.AddOrUpdate<IUserSynchronizer>("RedmineSync", r => r.SynchronizeAsync(), Cron.Daily(3), TimeZoneInfo.Local);
        }
    }
}