const {addPlugins, setOutput, webpack, createConfig, entryPoint} = require('@webpack-blocks/webpack');

const path = require("path");
const helpers = require("./helpers");
const basePreset = require("./presets/preset.env");

module.exports = createConfig([
    basePreset("vendor"),
    entryPoint({
        "polyfills": "Bundles/polyfills",
        "vendor": "Bundles/vendor",
    }),
    setOutput({
        library: '[name]_[hash]'
    }),

    addPlugins([
        new webpack.DllPlugin({
            path: path.join(helpers.outputRoot, '[name]-manifest.json'),
            name: '[name]_[hash]',
            context: "ClientApp"
        })
    ]),
]);