const {env, setOutput, createConfig, entryPoint, webpack, addPlugins} = require('@webpack-blocks/webpack');

const helpers = require("./helpers");
const CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
const basePreset = require("./presets/preset.env");
const addLoaders = require("./blocks/block.loaders");
const path = require('path');
const dllReference = require("./blocks/block.dll-reference");
const devServer = require('@webpack-blocks/dev-server');

module.exports = createConfig([
    basePreset("app"),
    entryPoint({
        "app": "boot",
        "bootstrap": "Styles/bootstrap.scss"
    }),
    env("development", [
        entryPoint([
            "webpack-dev-server/client",
            "webpack/hot/only-dev-server"
        ]),
        setOutput({
            publicPath: "http://localhost:7890/"
        }),
        devServer({
            port: 7890,
            contentBase: helpers.root("ClientApp"),
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
                "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
            }
        }),
        addLoaders([
            {
                test: /\.tsx?$/,
                loaders: ['react-hot', 'awesome-typescript'],
                // checker does not work. Need to use babel or switch to react-hot-loader v3
                // include: "ClientApp"
            }
        ])
    ]),
    dllReference("polyfills"),
    dllReference("vendor"),
]);