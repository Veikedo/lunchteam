module.exports = function (loaders) {
    return (context) => ({
        module: {
            loaders: loaders
        }
    })
};