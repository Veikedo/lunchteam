const helpers = require("./../helpers");
const {CheckerPlugin} = require('awesome-typescript-loader');

module.exports = function (options) {
    const {exclude = /\/node_modules\//} = options || {};

    return (context) => ({
        preLoaders: [{
            test: /\.tsx?$/,
            loader: "tslint-loader",
            exclude: [helpers.root("node_modules")]
        }],
        module: {
            loaders: [{
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader",
                exclude: Array.isArray(exclude) ? exclude : [exclude],
            }]
        },
        plugins: [
            new CheckerPlugin()
        ]
    })
};