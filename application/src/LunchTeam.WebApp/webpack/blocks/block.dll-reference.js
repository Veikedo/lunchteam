const helpers = require("./../helpers");
const path = require('path');
const webpack = require("webpack");

module.exports = function (manifest) {
    return (context) => ({
        plugins: [
            new webpack.DllReferencePlugin({
                context: helpers.root("ClientApp"),
                manifest: require(path.join(helpers.outputRoot, `${manifest}-manifest.json`)),
            }),
        ]
    })
};