"use strict";

const {addPlugins, defineConstants, group, setOutput, env, sourceMaps, webpack, customConfig} = require('@webpack-blocks/webpack');
const typescript = require('./../blocks/block.typescript.js');
const BuildNotifierPlugin = require("webpack-build-notifier");
const helpers = require("../helpers");
const NODE_ENV = process.env.NODE_ENV = process.env.NODE_ENV || "development";
const sass = require('@webpack-blocks/sass');
const extractText = require('@webpack-blocks/extract-text');
const postcss = require('@webpack-blocks/postcss');
const autoprefixer = require('autoprefixer')

module.exports = function (buildTitle) {
    return group([
        setOutput({
            path: helpers.outputRoot,
            filename: "[name].bundle.js",
            sourceMapFilename: "[name].map",
            chunkFilename: "[id].chunk.js",
        }),
        customConfig({
            resolve: {
                extensions: [".ts", ".tsx"],
                root: helpers.root("./ClientApp")
            }
        }),
        defineConstants({
            'process.env.NODE_ENV': `${NODE_ENV}`,
            'DEBUG': NODE_ENV === 'development',
            'PRODUCTION': NODE_ENV === 'production'
        }),
        typescript(),
        addPlugins([
            new webpack.NoErrorsPlugin(),
            new webpack.optimize.OccurenceOrderPlugin(true),
            new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/)
        ]),
        sass(),
        postcss([
            autoprefixer({browsers: ['last 4 versions']})
        ]),
        env("development", [
            sourceMaps("cheap-module-eval-source-map"),
            addPlugins([
                new BuildNotifierPlugin({
                    title: `${buildTitle}`,
                    suppressSuccess: true
                })
            ]),
            customConfig({
                debug: true,
            }),
        ]),
        env("production", [
            sourceMaps("source-map"),
            extractText('[name].css', 'text/x-sass'),
            addPlugins([
                new webpack.optimize.DedupePlugin(),
                new webpack.optimize.UglifyJsPlugin({
                    beautify: false,
                    comments: false,
                    mangle: {
                        screw_ie8: true,
                        keep_fnames: true
                    },
                    compress: {
                        screw_ie8: true,
                        warnings: false
                    },
                }),
            ])
        ]),
    ]);
};