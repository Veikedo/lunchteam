import {singleton} from "../Utils/di";
import {RouterStore} from "mobx-react-router";
import {computed, autorun} from "mobx";
import {ContestStore} from "./";
import * as $ from "jquery";

@singleton()
export class UiStore {
    constructor(private routerStore: RouterStore, private contestStore: ContestStore) {
        autorun(() => {
            $("body").toggleClass("--primary", this.isPassedRoute);
        });
    }

    hiddenUserAvatarContainer: any;

    @computed get isHomeRoute() {
        return this.routerStore.location.pathname == "/";
    }

    @computed get isPassedRoute() {
        return this.routerStore.location.pathname == "/contest" && this.contestStore.isCompleted;
    }
}