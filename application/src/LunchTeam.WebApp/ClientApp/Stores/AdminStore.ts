import {singleton} from "Utils/di";
import {AddNewContest, ContestService} from "Services";

@singleton()
export default class AdminStore {
    constructor(private contestService: ContestService) {
    }

    redmineSync = () => this.contestService.redmineSync();

    addNewContest = (params: AddNewContest) => this.contestService.addNewContest(params);

    completeCurrentContest = () => this.contestService.completeCurrentContest();

    notifyNewContestStarted = () => this.contestService.notifyNewContestStarted()
}