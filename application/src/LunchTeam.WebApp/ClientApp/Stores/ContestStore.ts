import {observable, computed, autorun} from "mobx";
import {singleton} from "Utils/di";
import {ContestService, ContestState} from "Services";
import {QuestionModel} from "Models/QuestionModel";
import {ContestInfoModel} from "Models/ContestInfoModel";

@singleton()
export class ContestStore {
    constructor(private contestService: ContestService) {
    }

    @observable contestInfo: ContestInfoModel;
    @observable contestState: ContestState;
    @observable questions: QuestionModel[];
    @observable isInitialized: boolean;
    @observable needToAnswerAboutCar: boolean;

    @computed get correctAnswers() {
        return this.questions.filter(q => q.isCorrect).length;
    }

    @computed get currentQuestionNumber() {
        const idx = this.questions && this.questions.findIndex(q => !q.isAnswered);
        return idx < 0 ? this.questions.length - 1 : idx;
    }

    @computed get currentQuestion() {
        return this.questions && this.questions.find(q => !q.isAnswered);
    }

    @computed get isStarted() {
        return this.contestState != null && this.contestState !== ContestState.NotStarted;
    }

    @computed get isCompleted() {
        return this.contestState === ContestState.Completed;
    }

    @computed get isFailed() {
        return this.contestState === ContestState.Lost;
    }

    async setCarAvailability(hasAuto: boolean) {
        await this.contestService.setAutoAvailability(hasAuto);
        this.needToAnswerAboutCar = false;
    }

    /**
     * Join the user to current contest and get questions.
     * Will only load questions if already joined
     * @returns {Promise<QuestionModel[]>}
     */
    async joinContest() {
        if (!this.questions) {
            const questions = await this.contestService.joinContest();
            this.questions = questions.map(q => new QuestionModel(q));
        }

        return this.questions;
    }

    loadQuestions() {
        return this.joinContest();
    }

    async initialize() {
        if (!this.isInitialized) {
            const contestInfo = await this.contestService.getContestInfo();
            this.contestInfo = new ContestInfoModel(contestInfo);

            this.contestState = await this.contestService.getUserContestState();
            autorun(this.syncState.bind(this));
        }

        this.isInitialized = true;
    }

    private async syncState() {
        if (!this.questions) {
            return;
        }

        let hasChanges = false;
        for (const question of this.questions) {
            if (question.answeredVariantId && !question.isAnswered) {
                const result = await this.contestService.answer(question.id, question.answeredVariantId);
                question.correctVariantId = result.correctVariantId;

                hasChanges = true;
            }
        }

        if (hasChanges) {
            const prev = this.contestState;
            this.contestState = await this.contestService.getUserContestState();

            const justCompleted = this.isCompleted && prev === ContestState.Started;
            if (justCompleted) {
                this.contestInfo.membersNumber++;
                this.needToAnswerAboutCar = true;
            }
        }
    }
}