import * as React from "react";
import * as ReactBootstrap from "react-bootstrap";
import ChangeEventHandler = React.ChangeEventHandler;
import RadioProps = ReactBootstrap.RadioProps;
import Radio = ReactBootstrap.Radio;

declare module "react-bootstrap" {
    import ReactElement = React.ReactElement;
    interface RadioProps {
        onChange?: ChangeEventHandler<HTMLInputElement>&ChangeEventHandler<Radio>;
    }

    interface PortalProps {
        container: () => ReactElement<any>;
    }
}

declare module "animate.css";