interface WebpackModule {
    hot;
    makeHot;
}

interface NodeModule extends WebpackModule {
}

declare module '*';