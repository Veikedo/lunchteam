declare module "mobx-remotedev" {
    interface IMobXRemoteDevConfig {
        /** the instance name to be showed on the monitor page. Default value is document.title. */
        name?: string;
        /**
         * set it to true to have a clear log only with actions. If MobX is in strict mode, it is true by default.
         * Don't forget about async actions.
         */
        onlyActions?: boolean;
        /**
         * set it to true in order to assign dispatching of all unhandled actions to this store. Useful for nested
         * classes / observables or when having async actions without specifying the scope explicitly.
         */
            global?: boolean;
        /** all other actions will be hidden in DevTools (the blacklist parameter will be ignored). */
        whitelist?: Array<RegExp | string>;
        /** set it to true to have remote monitoring via the local or remotedev.io server. */
        remote?: boolean;
        /** use to specify host for remotedev-server. If port is specified, default value is localhost. */
        hostname?: string;
        /** use to specify host's port for remotedev-server. */
        port?: number;
    }

    namespace mobXRemoteDev {
    }

    /**
     * Connect MobX Remote Dev
     *
     * @param store observable or class to be monitored. In case you want to change its values
     *      (to time travel or cancel actions), you should export its result as in the example
     *       above (so we can extend the class).
     * @param config Configuration
     *
     * @return store
     */
    function mobXRemoteDev<T>(store: T, config?: IMobXRemoteDevConfig): T;


    export = mobXRemoteDev;
}