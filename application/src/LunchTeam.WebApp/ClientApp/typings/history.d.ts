import {HistoryOptions} from "history";

declare module "history" {
    export function createHistory(HistoryOptions: HistoryOptions);
}