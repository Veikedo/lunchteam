import * as React from "react";
import {IndexRoute, Route} from "react-router";
import App from "./Components/App";
import Home from "./Components/Home";
import {Wizard} from "Components/Contest";
import About from "Components/About";
import NoContent from "Components/NoContent";
import Admin from "Components/Admin";

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
        <Route path="contest" component={Wizard}/>
        <Route path="about" component={About}/>
        <Route path="admin" component={Admin}/>
        <Route path="*" component={NoContent}/>
    </Route>
);


if (module.hot) {
    module.hot.accept();
}