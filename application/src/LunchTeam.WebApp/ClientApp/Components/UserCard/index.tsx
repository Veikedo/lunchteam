import * as React from "react";
import "./index.scss";

type Props = {
    hasAuto?: boolean;
    login: string;
    firstName: string;
    lastName: string;
    jobPosition: string;
}

export default function UserCard({ hasAuto, login, firstName, lastName, jobPosition }: Props) {
    const url = `api/avatar/user/${login}`;
    const fullName = `${firstName} ${lastName}`;

    return (
        <div className="user-card user-card-gradient">
            <div className="user-card-avatar">
                <img src={url} alt={fullName} className="avatar"/>
            </div>
            <div className="user-info">
                <div>
                    <span className="name">{firstName}</span><br />
                    <span className="name">{lastName}</span><br />
                    <span className="position">{jobPosition}</span>

                    { hasAuto && <div className="car"/> }
                </div>
            </div>
        </div>
    );
}

