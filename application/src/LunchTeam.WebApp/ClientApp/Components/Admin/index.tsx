import * as React from "react";
import {inject} from "Utils/di";
import AdminStore from "Stores/AdminStore";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import {observer} from "mobx-react";
import "./index.scss";
import {observable} from "mobx";
import AddNewContestModal from "./AddNewContestModal";

@observer
export default class Admin extends React.Component<{}, {}> {
    @inject() store: AdminStore;
    @observable isSyncingWithRedmine = false;
    @observable showAddNewContestModal = false;

    syncWithRedmine = async () => {
        try {
            this.isSyncingWithRedmine = true;
            await this.store.redmineSync();
            alert("Success: redmine sync")
        } catch (err) {
            alert("Fail: redmine sync");
            console.error(err);
        } finally {
            this.isSyncingWithRedmine = false;
        }
    };

    toggleAddNewContestModal = () => this.showAddNewContestModal = !this.showAddNewContestModal;

    render() {
        const { completeCurrentContest, notifyNewContestStarted } = this.store;

        return (
            <div className="admin">
                <AddNewContestModal
                    onClose={this.toggleAddNewContestModal}
                    showModal={this.showAddNewContestModal}
                />
                <ListGroup>
                    <ListGroupItem
                        header="Redmine"
                        onClick={this.isSyncingWithRedmine ? null : this.syncWithRedmine}
                        disabled={this.isSyncingWithRedmine}
                        bsStyle="success"
                    >
                        Synchronize members
                    </ListGroupItem>
                    <ListGroupItem
                        header="Contest"
                        onClick={this.toggleAddNewContestModal}
                        bsStyle="info"
                    >
                        Schedule a new one
                    </ListGroupItem>
                    <ListGroupItem
                        header="Newsletter"
                        bsStyle="warning"
                        onClick={notifyNewContestStarted}
                    >
                        Notify new contest started!
                    </ListGroupItem>
                    <ListGroupItem
                        header="Contest"
                        bsStyle="danger"
                        onClick={completeCurrentContest}
                    >
                        Complete it!
                    </ListGroupItem>
                </ListGroup>

            </div>
        );
    }
}