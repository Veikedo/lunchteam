import * as React from "react";
import {Button, Modal} from "react-bootstrap";
import "./index.scss";
import {observer} from "mobx-react";
import {FieldGroup} from "Components/Form";
import {inject} from "Utils/di";
import AdminStore from "Stores/AdminStore";
import {observable} from "mobx";
import * as moment from "moment";

type Props = {
    showModal: boolean,
    onClose: () => void;
};

@observer
export default class AddNewContestModal extends React.Component<Props, {}> {
    @inject() store: AdminStore;
    @observable dinnerDate: string;

    componentWillReceiveProps() {
        this.dinnerDate = moment().add(1, "weeks").weekday(4).format("MM-DD-YYYY");
    }

    addNewContest = async () => {
        const { dinnerDate } = this;
        await this.store.addNewContest({ dinnerDate });

        this.props.onClose();
    };

    render() {
        const { onClose } = this.props;
        return (
            <Modal show={this.props.showModal} onHide={onClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new contest</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <FieldGroup
                            value={this.dinnerDate}
                            label="Dinner date"
                            onChange={e => this.dinnerDate = e.target.value}
                        />
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="ordinal" onClick={this.addNewContest}>Add</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}