import * as React from "react";
import "./index.scss";
import * as classNames from "classnames";

type Props = React.HTMLProps<HTMLDivElement> & {
    type?: "hello"|"sad"|"play"|"cook"|"happy";
}
export default function Imp({ type, className, ...props }: Props) {
    const kindClass = `--${type || "hello"}`;
    return <div className={classNames("imp", kindClass, className)} {...props} />;
};