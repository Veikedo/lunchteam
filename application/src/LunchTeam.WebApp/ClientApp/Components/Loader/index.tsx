import * as React from "react";
import "./index.scss";
import * as classNames from "classnames";

type LoaderProps = {
    busy: boolean;
    style?: "large" | "";
    className?: string;
    children?;
};

export default function Loader({ busy, children, style = "", className, ...props }: LoaderProps) {
    if (busy) {
        return (
            <div
                className={classNames("loader-container animated fadeIn", className)}
                {...props}
            >
                <div className={classNames("loader", style)}>
                    <div className="rect1"/>
                    <div className="rect2"/>
                    <div className="rect3"/>
                    <div className="rect4"/>
                    <div className="rect5"/>
                </div>
            </div>
        );
    }

    return children || null;
}