import * as React from "react";
import {Button, Col, Row} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import "./Winners.scss";
import {observer} from "mobx-react";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";
import UserCard from "Components/UserCard";

@observer
export default class Winners extends React.Component<{}, {}> {
    @inject() store: ContestStore;

    render() {
        const { contestInfo } = this.store;

        return (
            <div className="winners">
                <h1>
                    <strong>{contestInfo.lastContestDinnerDate}</strong>
                </h1>
                <Row>
                    <Col xs={12}>
                        <Row className="user-cards">
                            {
                                this.store.contestInfo.lastContestWinners.map(item => (
                                    <Col key={item.login} xs={12} sm={6}>
                                        <UserCard {...item} />
                                    </Col>
                                ))
                            }
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className="text-center">
                        <LinkContainer to="/contest">
                            <Button bsStyle="primary">Участвовать</Button>
                        </LinkContainer>
                    </Col>
                </Row>
            </div>
        );
    }
}