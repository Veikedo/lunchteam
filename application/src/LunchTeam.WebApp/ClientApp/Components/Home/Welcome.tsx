import * as React from "react";
import "./Welcome.scss";
import {Button, OverlayTrigger, Tooltip} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import Imp from "../Imp";
import {observer} from "mobx-react";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";

@observer
export default class Welcome extends React.Component<{},{}> {
    @inject() store: ContestStore;

    render() {
        const { contestInfo } = this.store;

        const tooltip = (
            <Tooltip id="tooltip">
                Количество участников: <strong>{contestInfo.membersNumber}</strong><br/>
                {contestInfo.membersNumber === 0 && "Стань первым!"}
            </Tooltip>
        );

        return (
            <div className="welcome">
                <div className="billboard">
                    <div className="brand"/>
                    <span className="dash hidden-xs hidden-sm">&nbsp;&mdash;&nbsp;</span>
                    <div className="slogan">
                        <p>
                            Лучший повод узнать сотоварищей и отобедать<br />
                            Следующий обед <strong>{contestInfo.nextDinnerDate}</strong>
                        </p>
                        <LinkContainer to="/contest">
                            <OverlayTrigger placement="bottom" overlay={tooltip}>
                                <Button className="join">Участвую!</Button>
                            </OverlayTrigger>
                        </LinkContainer>
                    </div>
                </div>
                <Imp className="imp"/>
            </div>
        )
    }
};