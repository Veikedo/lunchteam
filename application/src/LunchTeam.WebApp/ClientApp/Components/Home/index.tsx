import * as React from "react";
import {observer} from "mobx-react";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";
import Welcome from "./Welcome";
import Winners from "./Winners";
import "./index.scss";

@observer
export default class Home extends React.Component<{}, {}> {
    @inject() store: ContestStore;

    render() {
        const { lastContestWinners } = this.store.contestInfo;
        return (
            <div className="home">
                <Welcome/>
                {lastContestWinners && !!lastContestWinners.length && <Winners/>}
            </div>
        );
    }
}