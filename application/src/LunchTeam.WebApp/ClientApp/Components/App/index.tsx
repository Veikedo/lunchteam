import * as React from "react";
import {Button, Row, Col, Grid} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import "./index.scss";
import {inject} from "Utils/di";
import {UiStore, ContestStore} from "Stores";
import * as classNames from "classnames";
import {Link} from "react-router";
import {observer} from "mobx-react";
import Loader from "../Loader";

@observer
export default class App extends React.Component<{},{}> {
    @inject() uiStore: UiStore;
    @inject() store: ContestStore;

    async componentDidMount() {
        await this.store.initialize();
    }

    render() {
        if (!this.store.isInitialized) {
            return <Loader busy style="large"/>;
        }

        const isHomeRoute = this.uiStore.isHomeRoute;
        const isPassedRoute = this.uiStore.isPassedRoute;

        return (
            <div className={classNames("app-container", isHomeRoute && "--with-lunch", isPassedRoute && "--primary")}>
                <div ref={c => this.uiStore.hiddenUserAvatarContainer = c}/>
                <Grid className="full-height">
                    <Row className="full-height">
                        <Col xs={12}>
                            <Link to="/" className="logo pull-left"/>
                            <LinkContainer to="/about">
                                <Button
                                    bsClass="btn-round"
                                    bsStyle={isHomeRoute || isPassedRoute ? "default" : "primary"}
                                    className="pull-right"
                                >
                                    ?
                                </Button>
                            </LinkContainer>
                        </Col>
                        <Col xs={12} className="children">
                            {this.props.children}
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}