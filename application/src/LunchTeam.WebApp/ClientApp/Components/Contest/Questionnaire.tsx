import * as React from "react";
import {Row, Col} from "react-bootstrap";
import {Question} from "./";
import {observer} from "mobx-react";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";
import {Stepper} from "./Stepper";
import "./Questionnaire.scss";
import {HiddenUserAvatar} from "./HiddenUserAvatar";

@observer
export class Questionnaire extends React.Component<{}, {}> {
    @inject() store: ContestStore;

    render() {
        const { questions, currentQuestionNumber, currentQuestion } = this.store;
        if (!currentQuestion) {
            return null;
        }

        const { variants } = currentQuestion;

        return (
            <div className="questionnaire">
                <Row>
                    <Col sm={6}>
                        <HiddenUserAvatar question={currentQuestion}/>
                    </Col>
                    <Col sm={6}>
                        <Stepper
                            questions={questions}
                            currentQuestion={currentQuestion}
                        />
                        <Question
                            variants={variants}
                            isLast={currentQuestionNumber == variants.length - 1}
                            onChosen={currentQuestion.answer}
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}