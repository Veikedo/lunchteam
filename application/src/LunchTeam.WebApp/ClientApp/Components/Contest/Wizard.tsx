import * as React from "react";
import {observable} from "mobx";
import {LetsPlay} from "./";
import {observer} from "mobx-react";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";
import Loader from "../Loader/index";
import {Questionnaire} from "./Questionnaire";
import {Failed} from "./Failed";
import {Passed} from "./Passed";

@observer
export class Wizard extends React.Component<{}, {}> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.joinContest = this.joinContest.bind(this);
    }

    @inject() store: ContestStore;
    @observable contestStarted: boolean;

    async componentDidMount() {
        this.contestStarted = this.store.isStarted;
        if (this.contestStarted) {
            await this.store.loadQuestions();
        }
    }

    async joinContest() {
        this.contestStarted = true;
        await this.store.joinContest();
    }

    render() {
        if (!this.contestStarted) {
            return <LetsPlay onStart={this.joinContest}/>
        }

        if (!this.store.questions) {
            return <Loader busy/>;
        }

        if (this.store.isCompleted) {
            return <Passed/>;
        }

        if (this.store.isFailed) {
            return <Failed/>
        }

        return <Questionnaire/>;
    }
}