import * as React from "react";
import "./Failed.scss";
import Imp from "../Imp";

export function Failed() {
    return (
        <div className="failed-contest">
            <div className="apologize">
                <h1>-__-</h1>
                <p>
                    Ты&nbsp;не&nbsp;узнал никого из&nbsp;попавшихся. <br />
                    Не&nbsp;расстраивайся, просто нужно было заглянуть в&nbsp;редмайн :) <br />
                    В&nbsp;любом случае, пообедать с&nbsp;коллегами&nbsp;— это хорошая идея. <br />
                    Пригласи кого-нибудь на&nbsp;обед&nbsp;и&nbsp;пробуй на&nbsp;следующей неделе!
                </p>
            </div>
            <Imp type="sad" className="imp"/>
        </div>
    )
}