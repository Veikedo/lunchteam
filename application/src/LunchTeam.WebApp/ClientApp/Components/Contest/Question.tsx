import * as React from "react";
import {Button, Radio} from "react-bootstrap";
import "./Question.scss";
import {observable} from "mobx";
import {observer} from "mobx-react";
import {Variant} from "Services";

type Props = {
    variants: Variant[],
    onChosen: (variantId: string) => any;
    isLast: boolean;
};

@observer
export class Question extends React.Component<Props, {}> {
    constructor(props: Props, context: any) {
        super(props, context);
        this.chosenVariantId = props.variants[0].id;
    }

    @observable chosenVariantId: string;

    componentWillReceiveProps({ variants }: Props) {
        this.chosenVariantId = variants[0].id;
    }

    render() {
        const { variants, onChosen, isLast } = this.props;

        return (
            <div className="question">
                <h1><strong>Кто на этом фото?</strong></h1>

                Выбери правильный вариант

                {
                    variants.map(i => (
                        <Radio
                            key={i.id}
                            name="variant"
                            value={i.id}
                            checked={i.id === this.chosenVariantId}
                            onChange={e => this.chosenVariantId = e.target.value}
                        >
                            <strong>{i.firstName} {i.lastName}</strong>, {i.jobPosition}
                        </Radio>
                    ))
                }

                <Button bsStyle="primary" onClick={e => onChosen(this.chosenVariantId)}>
                    {isLast ? "Завершить" : "Следующий"}
                </Button>
            </div>
        );
    }
}