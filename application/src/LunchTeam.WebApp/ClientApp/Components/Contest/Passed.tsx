import * as React from "react";
import "./Passed.scss";
import Imp from "../Imp/index";
import {inject} from "Utils/di";
import {ContestStore} from "Stores";
import {Button} from "react-bootstrap";
import {observer} from "mobx-react";

@observer
export class Passed extends React.Component<{},{}> {
    @inject() store: ContestStore;

    questionAbout() {
        const correctAnswers =  this.store.correctAnswers;
        const comrades =
                    correctAnswers === 1 ? "одного товарища"
                  : correctAnswers === 2 ? "двоих товарищей"
                  : correctAnswers === 3 ? "троих товарищей"
                  : "своих товарищей";

        return (
            <div>
                <h1>Ta-дам!</h1>
                <p>Ты узнал {comrades}!</p>
                <h2><strong>Последний вопрос, ты за рулём?</strong></h2>
                <p>Команде требуется транспорт, есть&nbsp;ли у&nbsp;тебя<br/>
                    возможность подвезти коллег?</p>
                <Button className="yes-no" onClick={() => this.store.setCarAvailability(true)}>Да</Button>
                <Button className="yes-no" onClick={() => this.store.setCarAvailability(false)}>Нет</Button>
            </div>
        )
    }

    bePatient() {
        return (
            <div>
                <h1>Спасибо, <br/> заявка принята!</h1>
                <p>Ещё совсем немного и&nbsp;искусственный интеллект определится с&nbsp;составом команды.<br/>
                    Будь терпелив, результаты соревнования придут в&nbsp;виде письма.</p>
            </div>
        )
    }

    render() {
        const needToAnswerAboutCar = this.store.needToAnswerAboutCar;

        return (
            <div className="passed-contest">
                <div className="congrats">
                    {needToAnswerAboutCar ? this.questionAbout() : this.bePatient()}
                </div>
                <Imp type="happy" className="imp"/>
            </div>
        );
    }
}