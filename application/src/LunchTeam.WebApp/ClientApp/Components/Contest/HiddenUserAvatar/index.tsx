import * as React from "react";
import {QuestionModel} from "Models/QuestionModel";
import {observer} from "mobx-react";
import "./index.scss";
import Loader from "Components/Loader/index";
import {Portal} from "react-overlays";
import {inject} from "Utils/di";
import {UiStore} from "Stores";
import * as classNames from "classnames";

type Props = {
    question: QuestionModel
}

const ImageLoader = () => (
    <div className="hidden-user-avatar">
        <Loader busy className="image-loader"/>
    </div>
);

const SmallAvatar = ({ src, isCropped }) => {
    const className = classNames("small-user-avatar user-card-gradient animated rollIn",
        isCropped && "hidden-md hidden-lg"
    );

    return (
        <div className={className}>
            <img src={src}/>
        </div>
    )
};

const LargeAvatar = ({ src, isCropped }) => {
    const style = { backgroundImage: `url('${src}')` };
    const className = classNames("large-user-avatar animated fadeIn",
        isCropped ? "hidden-xs hidden-sm" : "hidden"
    );

    return (
        <div className="hidden-user-avatar">
            <div className={className} style={style}/>
        </div>
    )
};

@observer
export class HiddenUserAvatar extends React.Component<Props, {}> {
    @inject() uiStore: UiStore;

    render() {
        const { question } = this.props;
        const avatar = question.avatar.current();

        if (!avatar) {
            return <ImageLoader/>
        }

        const src = `data:image;base64,${avatar.image}`;
        const isCropped = avatar.isCropped;

        return (
            <div className="hidden-user-avatar">
                <SmallAvatar src={src} isCropped={isCropped}/>

                <Portal container={() => this.uiStore.hiddenUserAvatarContainer}>
                    <LargeAvatar src={src} isCropped={isCropped}/>
                </Portal>
            </div>
        );
    }
}