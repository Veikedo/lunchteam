import * as React from "react";
import {Button} from "react-bootstrap";
import "./LetsPlay.scss";
import Imp from "../Imp/index";

type Props = {
    onStart: () => any;
}

export function LetsPlay({ onStart }: Props) {
    return (
        <div className="lets-play">
            <div className="start-contest">
                <h1>Квест!</h1>
                <p>
                    Мы почти внесли твою заявку, но прежде, давай поиграем.<br/>
                    Это займёт не более 30 секунд.
                </p>
                <Button bsStyle="primary" onClick={onStart}>Вперёд!</Button>
            </div>
            <Imp type="play" className="imp"/>
        </div>
    );
}
