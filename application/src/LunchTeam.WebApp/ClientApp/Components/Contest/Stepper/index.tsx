import * as React from "react";
import "./index.scss";
import {QuestionModel} from "../../../Models/QuestionModel";
import * as classNames from "classnames";

type Props = {
    questions: QuestionModel[];
    currentQuestion: QuestionModel;
}

export function Stepper({ questions, currentQuestion }:Props) {
    const current = currentQuestion.id;

    return (
        <ul className="stepper">
            {
                questions.map(q =>
                    <li
                        key={q.id}
                        className={classNames(q.id === current && "active", q.isAnswered && (q.isCorrect ? "passed":"failed")) }
                    />
                )
            }
        </ul>
    )
}
