import * as React from "react";
import "./index.scss";
import {Col, Row} from "react-bootstrap";
import UserCard from "Components/UserCard";

export default function About() {
    return (
        <div className="about">
            <div className="logo"/>
            <p>
                Нет лучше способа узнать ближе людей, с&nbsp;которыми мы&nbsp;работаем, чем сходить вместе на&nbsp;
                обед. Так давайте будем пользоваться им&nbsp;активнее!
                Компания не&nbsp;остается в&nbsp;стороне и&nbsp;берет на&nbsp;себя организаторскую стороны вопроса.
                Цель компании&nbsp;— перезнакомить всех со&nbsp;всеми. Нам нужен дружный и&nbsp;крепкий
                коллектив!</p>
            <ul>
                <li>
                    <i className="dash"/>
                    <p>Каждую рабочую пятницу компания оплачивает обед 4х&nbsp;сотрудников.
                        В&nbsp;целях благоразумности размер чека ограничен 1500р. Компенсация производится наличными
                        при предъявлении чека и&nbsp;фото вашей дружной компании за&nbsp;столом (попросите официанта
                        сделать фото).</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Сотрудники выбираются специальным алгоритмом из&nbsp;тех, кто подал заявки. Алгоритм
                        псевдослучайный, но&nbsp;учитывает некоторые параметры, такие как стаж работы сотрудника
                        в&nbsp;компании, дата последнего участия в&nbsp;конкурсе и&nbsp;другие.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Хотя&nbsp;бы один участник будет с&nbsp;автомобилем, чтобы добраться до&nbsp;места. В&nbsp;
                        заявке есть такой вопрос и&nbsp;это учитывается алгоритмом выбора кандидатов.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Место проведения&nbsp;— офис в&nbsp;Академгородке. При этом мы&nbsp;будем более чем рады,
                        если к&nbsp;нам присоединятся сотрудники и&nbsp;из&nbsp;других офисов.
                        Вы&nbsp;можете к&nbsp;нам приехать на&nbsp;один рабочий день, рабочее место будет
                        предоставлено.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Место обеда участники выбирают самостоятельно.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>В&nbsp;случае невозможности участия в&nbsp;уже запланированном обеде, необходимо сообщить
                        об&nbsp;этом организаторам как можно скорее.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Заявку на&nbsp;участие в&nbsp;следующем обеде можно сделать после проведения текущего,
                        но&nbsp;не&nbsp;позже чем за&nbsp;36 часов до&nbsp;начала.</p>
                </li>
                <li>
                    <i className="dash"/>
                    <p>Результаты оглашаются за&nbsp;24&nbsp;часа до&nbsp;проведения обеда.</p>
                </li>
            </ul>
            <h2>Контакты</h2>
            <Row>
                <Col sm={6}>
                    <p>
                        <strong>Компенсация и&nbsp;общие вопросы:</strong> <br />
                        <a href="mailto:amakosova@oneincsystems.com">Александра Макосова</a>
                    </p>
                </Col>
                <Col sm={6}>
                    <p>
                        <strong>Предложения и&nbsp;обратная связь</strong> <br />
                        <a href="mailto:vgorn@onesystems.ru">Виталий Горн</a>
                    </p>
                </Col>
            </Row>
            <h2>Проект воплотили в&nbsp;жизнь</h2>
            <Row>
                <Col sm={6}>
                    <UserCard
                        firstName="Kseniya"
                        lastName="Afrikantova"
                        login="kafrikantova"
                        jobPosition="Designer"
                    />
                </Col>
                <Col sm={6}>
                    <UserCard
                        firstName="Viktor"
                        lastName="Nikolaev"
                        login="vnikolaev"
                        jobPosition="Developer"
                    />
                </Col>
            </Row>
        </div>
    )
}