import * as React from "react";

const NoContent = () => (
    <div style={{ margin: "20px 0" }}>
        <h1>404</h1>
        <h2>Кажется, вы забрели не туда</h2>
        <a href="/">На главную!</a>
    </div>
);

export default NoContent;