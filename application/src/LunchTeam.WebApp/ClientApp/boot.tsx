import "./Bundles/polyfills";
import "./Bundles/vendor";
import * as React from "react";
import * as ReactDOM from "react-dom";
import {useStrict} from "mobx";
import {RouterStore, syncHistoryWithStore} from "mobx-react-router";
import {Router, useRouterHistory} from "react-router";
import {createHistory} from "history";
import {registerInstance} from "Utils/di";
import "./Styles/site.scss";
import "animate.css";
import "moment/locale/ru";
import * as moment from "moment";

moment.locale("ru");
moment.updateLocale("ru", {
    weekdays : {
        standalone: "воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split('_'),
        format: "воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split('_'),
        isFormat: /[Вв] dddd/
    }
});

const basename = window["reactRouterBaseUrl"];
const hashHistory = useRouterHistory(createHistory)({ basename });

const routingStore = new RouterStore();
const history = syncHistoryWithStore(hashHistory, routingStore);

registerInstance(routingStore);

useStrict(false);

import routes from "./routes";

ReactDOM.render(
    <Router history={history} children={routes}/>,
    document.getElementById("react-app")
);