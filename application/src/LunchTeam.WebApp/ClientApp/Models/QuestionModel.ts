import {observable, computed, extendObservable} from "mobx";
import {Variant, Question, ContestService, HiddenUserAvatar} from "Services";
import {inject} from "Utils/di";
import {lazyObservable} from "mobx-utils";

export class QuestionModel implements Question {
    constructor(question: Question) {
        extendObservable(this, question);
        this.answer = this.answer.bind(this);
    }

    id: string;

    @observable correctVariantId: string;
    @observable answeredVariantId: string;
    @observable variants: Variant[];
    avatar = lazyObservable<HiddenUserAvatar>(sink => this.service.getHiddenUserAvatar(this.id).then(x => sink(x)));

    @inject() private service: ContestService;

    @computed get isCorrect() {
        return this.isAnswered && this.correctVariantId == this.answeredVariantId;
    }

    @computed get isAnswered() {
        return this.correctVariantId != null;
    }

    answer(variantId: string) {
        this.answeredVariantId = variantId;
    }
}
