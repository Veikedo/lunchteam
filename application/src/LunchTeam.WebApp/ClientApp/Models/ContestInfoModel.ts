import {ContestInfo, Winner} from "Services";
import * as moment from "moment";

export class ContestInfoModel implements ContestInfo {
    constructor(info: ContestInfo) {
        Object.assign(this, info);
        this.nextDinnerDate = moment(info.nextDinnerDate).format("в dddd, D MMMM");

        if (info.lastContestDinnerDate) {
            const lastDinnerDate = moment(info.lastContestDinnerDate);
            const format = "В dddd, D MMMM " + (lastDinnerDate.isAfter() ? "обедают" : "обедали");
            this.lastContestDinnerDate = lastDinnerDate.format(format);
        }
    }

    nextDinnerDate: string;
    membersNumber: number;
    lastContestWinners: Winner[];
    lastContestDinnerDate: string;
}