import axios from "axios";
import {singleton} from "Utils/di";
import {AddNewContest, AnswerResult, ContestInfo, ContestState, HiddenUserAvatar, Question} from "./";

@singleton()
export class ContestService {
    getContestInfo() {
        return axios.get("/api/contest/current").then(q => q.data) as Promise<ContestInfo>;
    }

    getUserContestState() {
        return axios.get("/api/contest/current/my-status").then(q => q.data.state) as Promise<ContestState>;
    }

    getQuestions() {
        return axios.get("/api/contest/current/question").then(q => q.data.questions) as Promise<Question[]>;
    }

    answer(questionId: string, variantId: string) {
        const data = { questionId, variantId };
        return axios.post("/api/contest/current/question", data).then(q => q.data) as Promise<AnswerResult>;
    }

    async setAutoAvailability(hasAuto: boolean) {
        await axios.post("/api/contest/current/auto", { hasAuto });
    }

    /**
     * Join the user to current contest and get questions.
     * Will return questions if already joined
     */
    joinContest() {
        return axios.post("/api/contest/current/join").then(q => q.data.questions) as Promise<Question[]>;
    }

    getHiddenUserAvatar(questionId: string) {
        return axios.get(`/api/avatar/question/${questionId}`).then(q => q.data) as Promise<HiddenUserAvatar>;
    }

    redmineSync() {
        return axios.post("/api/admin/redmine/sync") as Promise<any>;
    }

    addNewContest(params: AddNewContest) {
        return axios.post("/api/admin/contest", params) as Promise<any>;
    }

    completeCurrentContest() {
        return axios.post("/api/admin/contest/current/complete") as Promise<any>;
    }

    notifyNewContestStarted() {
        return axios.post("/api/admin/newsletter/new-contest") as Promise<any>;
    }
}