export interface ContestInfo {
    nextDinnerDate: string;
    membersNumber: number;
    lastContestWinners: Winner[];
    lastContestDinnerDate: string;
}

export interface Winner {
    login: string;
    firstName: string;
    lastName: string;
    jobPosition: string;
    hasAuto: boolean;
}

export interface UserQuestionsQueryResult {
    questions: Question[];
}

export interface Question {
    id: string;
    correctVariantId: string;
    answeredVariantId: string;
    variants: Variant[];
}

export interface Variant {
    id: string;
    firstName: string;
    lastName: string;
    jobPosition: string;
}

export interface AnswerResult {
    isAnswerCorrect: boolean;
    correctVariantId: string;
}

export enum ContestState {
    NotStarted = 0,
    Started    = 1,
    Completed  = 2,
    Lost       = 3,
    Won        = 4
}

export interface UserContestStatus {
    state: ContestState;
}

export interface HiddenUserAvatar {
    isCropped: boolean;
    image: string;
}

export interface AddNewContest {
    dinnerDate: string;
}