import * as remdev from "mobx-remotedev";

/*
 * This decorator enables Redux devtools support in Chrome
 * */
export function remotedev() {
    return target => remdev(target, { global: true, name: target.name });
}

export function reduxSpy(store) {
    remdev(store, { global: false, name: store.constructor.name });
}