import * as inversify from "inversify";
import {Container, interfaces} from "inversify";
import getDecorators from "inversify-inject-decorators";
import {makeFluentProvideDecorator} from "inversify-binding-decorators";

export type ServiceIdentifier = string | symbol | interfaces.Newable<any> | interfaces.Abstract<any>;

const container = new Container();
const provide = makeFluentProvideDecorator(container);
const injectDecorator = getDecorators(container).lazyInject;

export function transient(serviceIdentifier?: ServiceIdentifier) {
    if (serviceIdentifier) {
        return provide(serviceIdentifier).inTransientScope().done();
    }

    return target => provide(target).inTransientScope().done()(target);
}

export function singleton(serviceIdentifier?: ServiceIdentifier) {
    if (serviceIdentifier) {
        return provide(serviceIdentifier).inSingletonScope().done();
    }

    return target => provide(target).inSingletonScope().done()(target);
}

export function registerInstance(instance: Object, serviceIdentifier?: ServiceIdentifier) {
    container.bind(serviceIdentifier || instance.constructor).toConstantValue(instance);
}

export function inject(serviceIdentifier?: ServiceIdentifier) {
    return function (target, propertyKey, parameterIndex?: number): any {
        if (parameterIndex !== undefined) {
            // use as parameter decorator
            inversify.inject(serviceIdentifier)(target, propertyKey, parameterIndex);
        } else if (serviceIdentifier) {
            return injectDecorator(serviceIdentifier);
        } else {
            const propType = Reflect.getMetadata("design:type", target, propertyKey);
            return injectDecorator(propType)(target, propertyKey);
        }
    };
}