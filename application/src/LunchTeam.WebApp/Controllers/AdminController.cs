﻿using System.Threading.Tasks;
using LunchTeam.Commands;
using LunchTeam.Queries.ContestInfo;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LunchTeam.WebApp.Controllers
{
    [Authorize("admin")]
    [Route("api/[controller]")]
    public class AdminController : Controller
    {
        private readonly IMediator _mediator;

        public AdminController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("redmine/sync")]
        public async Task<OkResult> SynchronizeWithRedmine()
        {
            var request = new StartSyncWithRedmine();
            await _mediator.Send(request).ConfigureAwait(false);

            return Ok();
        }

        [HttpPost("contest")]
        public async Task<OkResult> AddNewContest([FromBody] AddNewContest request)
        {
            await _mediator.Send(request).ConfigureAwait(false);
            return Ok();
        }

        [HttpPost("contest/current/complete")]
        public async Task<ContestInfoQueryResult> CompleteCurrentContest()
        {
            await _mediator.Send(new CompleteCurrentContest()).ConfigureAwait(false);
            return await _mediator.Send(new ContestInfoQuery()).ConfigureAwait(false);
        }

        [HttpPost("newsletter/new-contest")]
        public async Task<ActionResult> NotifyNewContestStarted()
        {
            await _mediator.Send(new NotifyNewContestStarted()).ConfigureAwait(false);
            return Ok();
        }
    }
}