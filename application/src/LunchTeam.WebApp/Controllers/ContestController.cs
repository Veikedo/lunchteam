﻿using System.Threading.Tasks;
using LunchTeam.Commands;
using LunchTeam.Domain;
using LunchTeam.Queries.ContestInfo;
using LunchTeam.Queries.CurrentContestMembers;
using LunchTeam.Queries.UserContestStatus;
using LunchTeam.Queries.UserQuestions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LunchTeam.WebApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ContestController : Controller
    {
        private readonly IMediator _mediator;

        public ContestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("current")]
        public Task<ContestInfoQueryResult> GetCurrentContestInfo()
        {
            return _mediator.Send(new ContestInfoQuery());
        }

        [HttpGet("current/my-status")]
        public Task<UserContestStatusResult> GetUserContestStatus()
        {
            return _mediator.Send(new UserContestStatusQuery());
        }

        [HttpPost("current/join")]
        public async Task<UserQuestionsQueryResult> AddUserToCurrentContest()
        {
            await _mediator.Send(new AddUserToCurrentContest()).ConfigureAwait(false);
            return await GetUserQuestions().ConfigureAwait(false);
        }

        [HttpGet("current/question")]
        public Task<UserQuestionsQueryResult> GetUserQuestions()
        {
            return _mediator.Send(new UserQuestionsQuery());
        }

        [HttpPost("current/question")]
        public Task<AnswerResult> Answer([FromBody] AnswerQuestion answerQuestion)
        {
            return _mediator.Send(answerQuestion);
        }

        [HttpPost("current/auto")]
        public Task SetAutoAvailability([FromBody] SetAutoAvailability setAutoAvailability)
        {
            return _mediator.Send(setAutoAvailability);
        }

        [HttpGet("current/members")]
        public Task GetCurrentContestMembers()
        {
            return _mediator.Send(new CurrentContestMembersQuery());
        }
    }
}