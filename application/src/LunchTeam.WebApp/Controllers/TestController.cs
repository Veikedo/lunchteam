﻿using System.Threading.Tasks;
using LunchTeam.Queries.UserQuestions;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LunchTeam.WebApp.Controllers
{
    [Authorize("admin")]
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly IMediator _mediator;

        public TestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public Task<object> Test()
        {
            return _mediator.Send(new TestQueryDb());
        }
    }
}