﻿using System;
using System.Threading.Tasks;
using AngleSharp.Network;
using LunchTeam.Queries.HiddenUserAvatar;
using LunchTeam.Services.Contracts;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LunchTeam.WebApp.Controllers
{
    [Route("api/[controller]")]
    public class AvatarController : Controller
    {
        private readonly IAvatarService _avatarService;
        private readonly IMediator _mediator;

        public AvatarController(IAvatarService avatarService, IMediator mediator)
        {
            _avatarService = avatarService;
            _mediator = mediator;
        }

        [HttpGet("user/{userLogin}")]
        public async Task<FileContentResult> GetUserAvatar(string userLogin)
        {
            var file = await _avatarService.GetUserAvatarAsync(userLogin)
                .ContinueWith(t => t.Result.Image)
                .ConfigureAwait(false);

            return File(file, MimeTypeNames.Png);
        }

        [Authorize]
        [HttpGet("question/{questionId}")]
        public Task<HiddenUserAvatar> GetHiddenUserAvatar(Guid questionId)
        {
            return _mediator.Send(new HiddenUserAvatarQuery(questionId));
        }
    }
}